<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'API\v1'], function(){
	Route::post('register', 'UserController@register');
	Route::post('login', 'UserController@login');
    Route::post('forgot_password', 'UserController@forgot_password');
	Route::get('staticPages', 'UserController@staticPages');
	
	Route::get('email/verify/{id}', 'VerificationApiController@verify')->name('verificationapi.verify');
    Route::get('email/resend', 'VerificationApiController@resend')->name('verificationapi.resend');

});

Route::group(['middleware' => ['auth:api', 'blockedUser'], 'namespace' => 'API\v1'], function(){
	Route::post('logout', 'UserController@logout');
	Route::post('change-password', 'UserController@changePassword');
	Route::post('update-profile_picture', 'UserController@updateProfilePicture');
	Route::post('update-profile_details', 'UserController@editProfileDetails');
	Route::post('contact-us', 'UserController@contactUs');
	Route::get('details', 'UserController@details');
	Route::post('addInformation', 'UserController@addInformation');
	Route::post('add-child', 'UserController@add_child');
	Route::post('edit-child', 'UserController@edit_child');
	Route::post('delete-child', 'UserController@delete_child');
	Route::get('get-notifications', 'UserController@getNotifications');
	Route::get('unread-count', 'UserController@unreadCount');
	
	Route::get('workoutPlansListing', 'WorkoutPlanController@workoutPlanList');
	Route::get('categoriesList', 'WorkoutPlanController@categoriesList');
	Route::post('add_workout_timings', 'WorkoutPlanController@addWorkoutTimings');
	Route::post('update_workout_timings', 'WorkoutPlanController@updateWorkoutTimings');
	Route::get('dashboard', 'WorkoutPlanController@dashboard');
	Route::get('workoutsByCategory', 'WorkoutPlanController@workoutsByCategory');
	Route::get('workoutsByRoutine', 'WorkoutPlanController@workoutsByRoutine');
	Route::get('workoutsByAgegroup', 'WorkoutPlanController@workoutsByAgegroup');
	
	Route::post('subscriptionPlans', 'SubscriptionController@subscriptionPlans');
	
	Route::post('new-forum', 'CommunityForumController@new_forum');
	Route::post('edit-forum', 'CommunityForumController@edit_forum');
	Route::post('delete-forum', 'CommunityForumController@delete_forum');
	Route::post('like-unlike-forum', 'CommunityForumController@like_unlike_forum');
	Route::post('add-comment', 'CommunityForumController@addComment');
	Route::post('delete-comment', 'CommunityForumController@deleteComment');
	Route::post('forum-listing', 'CommunityForumController@forum_listing');
	Route::post('comment-listing', 'CommunityForumController@comment_listing');
	Route::post('report_forum', 'CommunityForumController@report_forum');
	Route::post('report_comment', 'CommunityForumController@report_comment');
	Route::get('my_posts', 'CommunityForumController@my_posts');
	
});
