<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('admin');
Route::get('/reset-password', function () {
    return View::make('reset-password');
});

Auth::routes();

Route::group(['prefix' => 'admin', 'middleware' => ['auth']],function(){
	Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
	Route::get('users/blocked/{id}', 'UserController@block');
	Route::get('export', 'UserController@export')->name('export');
	Route::resource('users', 'UserController');
	Route::get('workoutPlans/deactivate/{id}', 'WorkoutPlanController@deactivate');
	Route::get('workoutPlans/getSubcategory', 'WorkoutPlanController@getSubcategory')->name('getSubcategory');
	Route::resource('workoutPlans', 'WorkoutPlanController');
	Route::resource('cms', 'CmsPageController');
	Route::get('edit-profile', 'ProfileController@editProfile')->name('edit.profile');
	Route::put('update-profile', 'ProfileController@updateProfile')->name('update.profile');
	Route::get('change-password', 'ProfileController@changePassword')->name('change.password');
	Route::put('update-password', 'ProfileController@updatePassword')->name('update.password');
	Route::resource('subscriptions', 'SubscriptionController');
	Route::resource('forums', 'CommunityBoardController');
	Route::post('forum-comment-delete/{id}', 'CommunityBoardController@deleteComment')->name('delete-comment');
	Route::resource('contactUs', 'InquiryController');
	Route::resource('categories', 'CategoryController');
	Route::resource('subcategories', 'SubcategoryController');
	Route::resource('ageGroups', 'AgeGroupController');
	Route::resource('routines', 'RoutineController');
	Route::resource('reported-forums', 'ReportedForumController');
});

Route::get('cms/{slug}', 'CmsPageController@view_cms');
