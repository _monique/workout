<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('admin/assets/images/favicon.png') }}">
        <title>Workout - Admin Panel</title>        

        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

        <!-- Styles -->
       
        <link href="{{ asset('admin/dist/css/style.min.css') }}" rel="stylesheet">
        <link href="{{ asset('admin/dist/css/style.css') }}" rel="stylesheet">
        <link href="{{ asset('admin/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css') }}" rel="stylesheet">
        <script src="{{ asset('admin/assets/libs/jquery/dist/jquery.min.js') }}"></script>
        <script src="{{ asset('admin/assets/libs/popper.js/dist/umd/popper.min.js') }}"></script>
        <script src="{{ asset('admin/assets/libs/bootstrap/dist/js/bootstrap.min.js') }}"></script>
        @yield('top-css')
    </head>
    <body>         
        <div id="main-wrapper">
            <div class="preloader">
                <div class="lds-ripple">
                    <div class="lds-pos"></div>
                    <div class="lds-pos"></div>
                </div>
            </div>
            @auth                
                @include('partials.top-menu')
                @include('partials.left-menu')
            @endauth
        
            @yield('content')            
        </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
        <script src="{{ asset('admin/dist/js/sidebarmenu.js') }}"></script>
        <script src="{{ asset('admin/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js') }}"></script>
        <script src="{{ asset('admin/dist/js/custom.min.js') }}"></script>
        <script src="{{ asset('admin/assets/extra-libs/DataTables/datatables.min.js') }}"></script>
         
        <script type="text/javascript">
            $('[data-toggle="tooltip"]').tooltip();
            $(".preloader").fadeOut();
            // $('#zero_config').DataTable();
            $(document).ready(function(){
                  $(".alert").delay(5000).slideUp(300);
            });
        </script>
        @yield('footer-script')
    </body>
</html>