<style>
.cms-content-wrap{
	padding: 0 20px;
}
.cms-content-wrap:before {
    content: '';
    width: 100vw;
    height: 100vh;
    position: fixed;
    top: 0;
	left: 0;
    background: linear-gradient(to top, #9e89e6, #9e89e6, #86b2ef, #86b2ef);
    z-index: -1;
}
.cms-content {
    background: #fff;
    border-radius: 10px;
    margin: 30px auto 50px;
    padding: 15px 25px;
}
.cms-content p {
    word-break: break-word;
}
</style>

@extends('layouts.default')

@section('content')
@if($cms)
	<div class="cms-content-wrap">
		<div class="container cms-content">
	  	{!! $cms->content !!}
		</div>
	</div>
@else
	<div class="jumbotron text-center">
	  	<h1>Page content not found.</h1>
	  	<hr>
	</div>
@endif

@endsection
