@extends('layouts.web')

@section('top-css')
<style type="text/css">
.page-breadcrumb .d-flex {
    justify-content: space-between;
}
</style>
@endsection

@section('content')
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Add CMS Page</h4>
                <a href="{{ route('cms.index') }}" class="btn btn-primary float-right"><i class="fa fa-arrow-left"></i> Back</a>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <form class="form-horizontal" action="{{ route('cms.store') }}" enctype="multipart/form-data" method="post">
            @csrf
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Add CMS Page</h4>
                            <div class="form-group row">
                                <label for="title" class="col-sm-3 text-right control-label col-form-label">Title </label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="title" value="{{ old('title') }}" id="title" placeholder="Title">
                                    <span class="text-danger image_error">
                                        <strong>{{ $errors->has('title')?$errors->first('title'):'' }}</strong>
                                    </span>
                                </div>   
                            </div>
                            <div class="form-group row">
                                <label for="content" class="col-sm-3 text-right control-label col-form-label">Content</label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" name="content" id="content" placeholder="Full Description" rows="5">{{ old('content') }}</textarea>
                                    <span class="text-danger image_error">
                                        <strong>{{ $errors->has('content')?$errors->first('content'):'' }}</strong>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="border-top">
                    <div class="card-body">
                        <button class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@section('footer-script')
<script src="https://cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace( 'content' );
</script>
@endsection