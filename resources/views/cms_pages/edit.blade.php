@extends('layouts.web')

@section('top-css')
<style type="text/css">
img{
  max-width:180px;
}
input[type=file]{
    padding:20px;
}
</style>
@endsection

@section('content')
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Edit Workout Plan</h4>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <form class="form-horizontal" action="{{ route('cms.update' , $cms->id) }}" enctype="multipart/form-data" method="post">
            @csrf
            @method('put')
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Edit Workout Plan</h4>
                            <div class="form-group row">
                                <label for="title" class="col-sm-3 text-right control-label col-form-label">Title </label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="title" value="{{ old('title', $cms->title) }}" id="title" placeholder="Title">
                                    <span class="text-danger image_error">
                                        <strong>{{ $errors->has('title')?$errors->first('title'):'' }}</strong>
                                    </span>
                                </div>   
                            </div>
                            <div class="form-group row">
                                <label for="content" class="col-sm-3 text-right control-label col-form-label">Content</label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" name="content" id="content" placeholder="Full Description" rows="5">{{ old('content',$cms->content) }}</textarea>
                                    <span class="text-danger image_error">
                                        <strong>{{ $errors->has('content')?$errors->first('content'):'' }}</strong>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="border-top">
                    <div class="card-body">
                        <button class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@section('footer-script')
<script src="https://cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
<script type="text/javascript">
    CKEDITOR.replace( 'content' );

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah')
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
@endsection
