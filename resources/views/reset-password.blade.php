@extends('layouts.default')

@section('content')
<div class="page-wrapper">
     <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">You have successfully changed your password.</h4>
            </div>
        </div>
    </div>
</div>
@endsection
