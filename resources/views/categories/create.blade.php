@extends('layouts.web')

@section('top-css')
<style type="text/css">
img{
  max-width:180px;
  max-height:180px;
}
input[type=file]{
    padding:20px;
}
.page-breadcrumb .d-flex {
    justify-content: space-between;
}
</style>
@endsection

@section('content')
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Add Category</h4>
                <a href="{{ route('categories.index') }}" class="btn btn-primary float-right"><i class="fa fa-arrow-left"></i> Back</a>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <form class="form-horizontal" action="{{ route('categories.store') }}" enctype="multipart/form-data" method="post">
            @csrf
            <div class="card row">
                <div class="col-md-12">
                    <div class="card-body">
                        <h4 class="card-title">Add Category</h4>
                        <div class="form-group row">
                            <label for="name" class="col-sm-3 text-right control-label col-form-label">Image</label>
                            <div class="col-sm-9">
                                <img id="blah" src="{{ asset('/categories/default.png') }}" alt="Image" />
                                    <br>
                                <input type='file' name="image" onchange="readURL(this);" />
                                <br>
                                <span class="text-danger image_error">
                                    <strong>{{ $errors->has('image')?$errors->first('image'):'' }}</strong>
                                </span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-sm-3 text-right control-label col-form-label">Name</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="name" value="{{ old('name') }}" id="name" placeholder="Full Name">
                                <span class="text-danger image_error">
                                    <strong>{{ $errors->has('name')?$errors->first('name'):'' }}</strong>
                                </span>
                            </div>   
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-sm-3 text-right control-label col-form-label">Description</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="description" value="{{ old('description') }}" id="description" placeholder="Description">
                                <span class="text-danger image_error">
                                    <strong>{{ $errors->has('description')?$errors->first('description'):'' }}</strong>
                                </span>
                            </div>   
                        </div>
                    </div>
                </div>
                <div class="border-top">
                    <div class="card-body">
                        <button class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
@section('footer-script')
<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah')
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    
</script>
@endsection