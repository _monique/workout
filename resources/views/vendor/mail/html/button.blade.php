<table class="action" align="center" width="100%" cellpadding="0" cellspacing="0" role="presentation">
<tr>
<td align="center">
<table width="100%" border="0" cellpadding="0" cellspacing="0" role="presentation">
<tr>
<td align="center">
<table border="0" cellpadding="0" cellspacing="0" role="presentation">
<tr>
<td>
<!--<a href="{{ $url }}" class="button button-{{ $color ?? 'primary' }}" target="_blank">{{ $slot }}</a>-->
<a href="{{ $url }}" class="button button-{{ $color ?? 'primary' }}" target="_blank" style="text-decoration: none;background: linear-gradient(to top, #9e89e6, #86b2ef);color: #fff;font-size: 20px;padding: 7px 15px;display: inline-block;border-radius: 5px;margin: 5px 0;">{{ $slot }}</a>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
