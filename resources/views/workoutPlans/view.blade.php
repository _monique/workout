@extends('layouts.web')

@section('content')
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">View Workout Plan</h4>
            </div>
        </div>
    </div>
    <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">View Workout Plan</h4>
                            <div class="form-group row">
                                <label for="name" class="col-sm-3 text-right control-label col-form-label">Image/Video</label>                                
                                <div class="col-sm-9">
                                    <video id="blah" width="320" height="240" controls>
                                          <source src="{{ asset('/workoutMedia/'.$workoutPlan->media) }}" type="video/mp4">
                                        Your browser does not support the video tag.
                                    </video>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="category" class="col-sm-3 text-right control-label col-form-label">Category</label>
                                <div class="col-sm-9 col-form-label">
                                    {{ $workoutPlan->category->name }}
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="category" class="col-sm-3 text-right control-label col-form-label">Sub Category</label>
                                <div class="col-sm-9 col-form-label">
                                    @foreach($subcategories as $subcategory){{ $subcategory .', ' }} @endforeach
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="category" class="col-sm-3 text-right control-label col-form-label">Routines</label>
                                <div class="col-sm-9 col-form-label">
                                    @foreach($routines as $routine){{ $routine .', ' }} @endforeach
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="category" class="col-sm-3 text-right control-label col-form-label">Age Groups</label>
                                <div class="col-sm-9 col-form-label">
                                    @foreach($ageGroups as $ageGroup){{ $ageGroup .', ' }} @endforeach
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-sm-3 text-right control-label col-form-label">Title </label>
                                <div class="col-sm-9 col-form-label">
                                    {{$workoutPlan->name}}
                                </div>   
                            </div>
                            <div class="form-group row">
                                <label for="short_description" class="col-sm-3 text-right control-label col-form-label">Full Description</label>
                                <div class="col-sm-9 col-form-label">
                                    {{$workoutPlan->short_description}}
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="duration" class="col-sm-3 text-right control-label col-form-label">Time Duration(hrs) </label>
                                <div class="col-sm-9 col-form-label">
                                    {{$workoutPlan->duration}}
                                </div>   
                            </div>
                            <div class="form-group row">
                                <label for="calories_burn" class="col-sm-3 text-right control-label col-form-label">Caleories burn</label>
                                <div class="col-sm-9 col-form-label">
                                    {{$workoutPlan->calories_burn}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>
@endsection
@section('footer-script')
<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah')
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
@endsection