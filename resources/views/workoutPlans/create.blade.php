@extends('layouts.web')

@section('top-css')
<style type="text/css">
.page-breadcrumb .d-flex {
    justify-content: space-between;
}
</style>
@endsection

@section('content')
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Add Workout Plan</h4>
                <a href="{{ route('workoutPlans.index') }}" class="btn btn-primary float-right"><i class="fa fa-arrow-left"></i> Back</a>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <form class="form-horizontal" action="{{ route('workoutPlans.store') }}" enctype="multipart/form-data" method="post">
            @csrf
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Add Workout Plan</h4>
                            <div class="form-group row">
                                <label for="name" class="col-sm-3 text-right control-label col-form-label">Image/Video</label>
                                <div class="col-sm-9">
                                    <input type='file' name="media" value="{{ old('media') }}"/>
                                    <br>
                                    <span class="text-danger image_error">
                                        <strong>{{ $errors->has('media')?$errors->first('media'):'' }}</strong>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="category_id" class="col-sm-3 text-right control-label col-form-label">Category:</label>
                                <div class="col-sm-9">
                                    <select name="category_id" id="category_id" class="demoInputBox select2 form-control " onChange="getSubcategory(this.value);">
                                        <option value disabled selected>Select Category</option>
                                        @foreach ($categories as $key => $category) {
                                            <option value="{{$key}}">{{$category}}</option>
                                        @endforeach
                                    </select>
                                    <span class="text-danger image_error">
                                        <strong>{{ $errors->has('category_id')?$errors->first('category_id'):'' }}</strong>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="subcategory_id" class="col-sm-3 text-right control-label col-form-label">Sub Category:</label>
                                <div class="col-sm-9">
                                    <select name="subcategory_id[]" id="subcategory-list" multiple='multiple' class="demoInputBox select2 form-control" >
                                        
                                    </select>
                                    <span class="text-danger image_error">
                                        <strong>{{ $errors->has('subcategory_id')?$errors->first('subcategory_id'):'' }}</strong>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-sm-3 text-right control-label col-form-label">Title </label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="name" value="{{ old('name') }}" id="name" placeholder="Title">
                                    <span class="text-danger image_error">
                                        <strong>{{ $errors->has('name')?$errors->first('name'):'' }}</strong>
                                    </span>
                                </div>   
                            </div>
                            <div class="form-group row">
                                <label for="short_description" class="col-sm-3 text-right control-label col-form-label">Full Description</label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" name="short_description" id="short_description" placeholder="Full Description" rows="5">{{ old('short_description') }}</textarea>
                                    <span class="text-danger image_error">
                                        <strong>{{ $errors->has('short_description')?$errors->first('short_description'):'' }}</strong>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="age_group_id" class="col-sm-3 text-right control-label col-form-label">Age Group</label>
                                <div class="col-sm-9">
                                    <select class="select2 form-control" style="width: 100%; height:36px;" name="age_group_id[]" multiple='multiple'>
                                        @foreach($ageGroups as $key => $ageGroup)
                                        <option value="{{$key}}" {{ old('age_group_id') == $key ? 'selected' : '' }}>{{$ageGroup}}</option>
                                        @endforeach
                                    </select>
                                    <span class="text-danger image_error">
                                        <strong>{{ $errors->has('age_group_id')?$errors->first('age_group_id'):'' }}</strong>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="routine_id" class="col-sm-3 text-right control-label col-form-label">Routine</label>
                                <div class="col-sm-9">
                                    <select class="select2 form-control" name="routine_id[]" multiple='multiple'>
                                        @foreach($routines as $key => $routine)
                                        <option value="{{$key}}" {{ old('routine_id') == $key ? 'selected' : '' }}>{{$routine}}</option>
                                        @endforeach
                                    </select>
                                    <span class="text-danger image_error">
                                        <strong>{{ $errors->has('category_id')?$errors->first('category_id'):'' }}</strong>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="duration" class="col-sm-3 text-right control-label col-form-label">Time Duration(hrs) </label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="duration" value="{{ old('duration') }}" id="duration" placeholder="Time Duration">
                                    <span class="text-danger image_error">
                                        <strong>{{ $errors->has('duration')?$errors->first('duration'):'' }}</strong>
                                    </span>
                                </div>   
                            </div>
                            <div class="form-group row">
                                <label for="calories_burn" class="col-sm-3 text-right control-label col-form-label">Calories burn</label>
                                <div class="col-sm-9 row">
                                    <input type="text" class="form-control col-sm-6" name="calories" value="{{ old('calories') }}" placeholder="Calories Burn">
                                    <select class="form-control col-sm-6" style="width: 100%; height:36px;" name="calories_burn">
                                        <option value="cal" {{ old('calories_burn') == 'cal' ? 'selected' : '' }}>cal</option>
                                        <option value="kcal" {{ old('calories_burn') == 'kcal' ? 'selected' : '' }}>kcal</option>
                                        <option value="kj" {{ old('calories_burn') == 'kj' ? 'selected' : '' }}>kj</option>
                                    </select>
                                    <span class="text-danger image_error">
                                        <strong>{{ $errors->has('calories')?$errors->first('calories'):'' }}</strong>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="border-top">
                    <div class="card-body">
                        <button class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@section('footer-script')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.1.0-beta.1/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.1.0-beta.1/js/select2.min.js"></script>
<script type="text/javascript">
    function getSubcategory(val) {
    	$.ajax({
    		type: "GET",
    		url: "{{ route('getSubcategory') }}",
    		data:'category_id='+val,
    		beforeSend: function() {
    			$("#subcategory-list").addClass("loader");
    		},
    		success: function(data){
    		    $("#subcategory-list").html('');
    		    $(data).each(function (i) { //populate child options 
            		$("#subcategory-list").append("<option value=\""+data[i].id+"\">"+data[i].name+"</option>");
            	});
    			$("#subcategory-list").removeClass("loader");
    		}
    	});
    }
    $('.select2').select2({
       placeholder: {
        id: '-1',
        text: '-- Select --'
      }
    });
</script>
@endsection