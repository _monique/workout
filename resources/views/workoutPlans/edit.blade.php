@extends('layouts.web')

@section('top-css')
<style type="text/css">
img{
  max-width:180px;
}
input[type=file]{
    padding:20px;
}
</style>
@endsection

@section('content')
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Edit Workout Plan</h4>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <form class="form-horizontal" action="{{ route('workoutPlans.update' , $workoutPlan->id) }}" enctype="multipart/form-data" method="post">
            @csrf
            @method('put')
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Edit Workout Plan</h4>
                            <div class="form-group row">
                                <label for="name" class="col-sm-3 text-right control-label col-form-label">Image/Video</label>                                
                                <div class="col-sm-9">
                                    @if($workoutPlan->media == null)
                                    <img id="blah" src="{{ asset('/admin/assets/images/users/1.jpg') }}" alt="Media" />
                                    @else
                                    <video id="blah" width="320" height="240" controls>
                                          <source src="{{ asset('/workoutMedia/'.$workoutPlan->media) }}" type="video/mp4">
                                        Your browser does not support the video tag.
                                    </video>
                                    @endif
                                    <br>
                                    <input type='file' name="media" value="{{ old('media') }}" onchange="readURL(this);"/>
                                    <br>
                                    <span class="text-danger image_error">
                                        <strong>{{ $errors->has('media')?$errors->first('media'):'' }}</strong>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="category_id" class="col-sm-3 text-right control-label col-form-label">Category:</label>
                                <div class="col-sm-9">
                                    <select name="category_id" id="category_id" class="demoInputBox select2 form-control" onChange="getSubcategory(this.value);">
                                        <option value disabled selected>Select Category</option>
                                        @foreach ($categories as $key => $category) {
                                            <option value="{{$key}}" {{ old('category_id', $workoutPlan->category_id) == $key ? 'selected' : '' }}>{{$category}}</option>
                                        @endforeach
                                    </select>
                                    <span class="text-danger image_error">
                                        <strong>{{ $errors->has('category_id')?$errors->first('category_id'):'' }}</strong>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="subcategory_id" class="col-sm-3 text-right control-label col-form-label">Sub Category:</label>
                                <div class="col-sm-9">
                                    <select name="subcategory_id[]" id="subcategory-list" class="demoInputBox select2 form-control" multiple= "multiple" >
                                        @foreach ($subcategories as $key => $subcategory) {
                                            <option value="{{$key}}" {{ (in_array($key, explode(',', $workoutPlan->subcategory_id))) ? 'selected' :''}}> {{$subcategory}}</option>
                                        @endforeach
                                    </select>
                                    <span class="text-danger image_error">
                                        <strong>{{ $errors->has('subcategory_id')?$errors->first('subcategory_id'):'' }}</strong>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-sm-3 text-right control-label col-form-label">Title </label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="name" value="{{ old('name', $workoutPlan->name) }}" id="name" placeholder="Workout Name">
                                    <span class="text-danger image_error">
                                        <strong>{{ $errors->has('name')?$errors->first('name'):'' }}</strong>
                                    </span>
                                </div>   
                            </div>
                            <div class="form-group row">
                                <label for="short_description" class="col-sm-3 text-right control-label col-form-label">Full Description</label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" name="short_description" id="short_description" placeholder="Full Description" rows="5">{{ old('short_description',$workoutPlan->short_description) }}</textarea>
                                    <span class="text-danger image_error">
                                        <strong>{{ $errors->has('short_description')?$errors->first('short_description'):'' }}</strong>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="age_group_id" class="col-sm-3 text-right control-label col-form-label">Age Group</label>
                                <div class="col-sm-9">
                                    <select class="select2 form-control" name="age_group_id[]" multiple="multiple">
                                        <option value="">Select</option>
                                        @foreach($ageGroups as $key => $ageGroup)
                                        <option value="{{$key}}" {{ (in_array($key, explode(',', $workoutPlan->age_group_id))) ? 'selected' :''}}> {{$ageGroup}}</option>
                                        @endforeach
                                    </select>
                                    <span class="text-danger image_error">
                                        <strong>{{ $errors->has('age_group_id')?$errors->first('age_group_id'):'' }}</strong>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="routine_id" class="col-sm-3 text-right control-label col-form-label">Routine</label>
                                <div class="col-sm-9">
                                    <select class="select2 form-control" name="routine_id[]" multiple="multiple">
                                        <option value="">Select</option>
                                        @foreach($routines as $key => $routine)
                                        <option value="{{$key}}" {{ (in_array($key, explode(',', $workoutPlan->routine_id))) ? 'selected' :''}} >{{$routine}}</option>
                                        @endforeach
                                    </select>
                                    <span class="text-danger image_error">
                                        <strong>{{ $errors->has('routine_id')?$errors->first('routine_id'):'' }}</strong>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="duration" class="col-sm-3 text-right control-label col-form-label">Time Duration(hrs) </label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="duration" value="{{ old('duration', $workoutPlan->duration) }}" id="duration" placeholder="Time Duration">
                                    <span class="text-danger image_error">
                                        <strong>{{ $errors->has('duration')?$errors->first('duration'):'' }}</strong>
                                    </span>
                                </div>   
                            </div>
                            <div class="form-group row">
                                <label for="calories_burn" class="col-sm-3 text-right control-label col-form-label">Calories burn</label>
                                <div class="col-sm-9 row">
                                    <input type="text" class="form-control col-sm-6" name="calories" value="{{ old('calories', $calories[0]) }}" placeholder="Calories Burn">
                                    <select class="form-control col-sm-6" name="calories_burn">
                                        <option value="cal" {{ old('calories_burn', $calories[1]) == 'cal' ? 'selected' : '' }}>cal</option>
                                        <option value="kcal" {{ old('calories_burn', $calories[1]) == 'kcal' ? 'selected' : '' }}>kcal</option>
                                        <option value="kj" {{ old('calories_burn', $calories[1]) == 'kj' ? 'selected' : '' }}>kj</option>
                                    </select>
                                    <span class="text-danger image_error">
                                        <strong>{{ $errors->has('calories')?$errors->first('calories'):'' }}</strong>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="border-top">
                    <div class="card-body">
                        <button class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
@section('footer-script')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.1.0-beta.1/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.1.0-beta.1/js/select2.min.js"></script>
<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah')
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    function getSubcategory(val) {
    	$.ajax({
    		type: "GET",
    		url: "{{ route('getSubcategory') }}",
    		data:'category_id='+val,
    		beforeSend: function() {
    			$("#subcategory-list").addClass("loader");
    		},
    		success: function(data){
    		    $("#subcategory-list").html('');
    		    $(data).each(function (i) { //populate child options 
            		$("#subcategory-list").append("<option value=\""+data[i].id+"\">"+data[i].name+"</option>");
            	});
    			$("#subcategory-list").removeClass("loader");
    		}
    	});
    }
    	
    $('.select2').select2({
       placeholder: {
        id: '-1',
        text: '-- Select --'
      }
    });
</script>
@endsection