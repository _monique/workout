@extends('layouts.web')
@section('content')
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Video Management</h4>
            </div>
        </div>
    </div>
    <div class="container-fluid">
    	@include('flash::message')
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">WorkOut Plans
	                        <div class="text-right">
	                            <a href="{{ route('workoutPlans.create') }}" type="button" class="btn btn-success">Add</a>
	                        </div>
	                    </h5>
                        <div class="table-responsive">
                            <table id="zero_config" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>S.No.</th>
                                        <th>Title</th>
                                        <th>Category</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($workoutPlans as $key => $workoutPlan)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>                                 
                                        <td>{{ $workoutPlan->name }}</td>  
                                        <td>{{ $workoutPlan->category->name }}</td>  
                                        <td>
                                        	<a href="{{ url('admin/workoutPlans/deactivate/'.$workoutPlan->id) }}">
							                    @if($workoutPlan->is_activated)
							                    	<button type="button" class="btn-sm btn-success" title="De-Activate"><i class="fa fa-ban"></i> De-Activate</button>
							                	@else
							                		<button type="button" class="btn-sm btn-danger" title="Activate"><i class="fa fa-unlock"></i> Activate</button>
							            		@endif
	                                        </a>
		                                </td>                                     
                                        <td class="">
                                            <div class="comment-footer">
                                                <a href="{{ route('workoutPlans.edit', $workoutPlan->id) }}" type="button" class="btn btn-cyan btn-sm">Edit</a>
                                                <a href="{{ route('workoutPlans.show', $workoutPlan->id) }}" type="button" class="btn btn-info btn-sm">View</a>
                                                <form action="{{ route('workoutPlans.destroy', $workoutPlan->id) }}" method="POST" onsubmit="deleteWorkoutPlan('{{ $workoutPlan->id }}', '{{ $workoutPlan->name }}', event,this)">
                                                @csrf
                                                    <button class="btn-sm btn-danger" title="">DELETE</button>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>S.No.</th>
                                        <th>Name</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer-script')
<script type="text/javascript">
    $(document).ready( function () {
        $('#zero_config').DataTable({
            "columnDefs": [{ 'orderable': false, 'targets': 4 }],
        });
    });

    function deleteWorkoutPlan(id, title, event,form)
    {
        event.preventDefault();
        swal({
            title: "Are you sure?",
            text: "You want to delete "+title+" plan",
            icon: "warning",
            buttons: {
                cancel: true,
                confirm: true,
            },
            closeModal: false,
            closeModal: false,
            closeOnEsc: false,
        })
       .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                url: $(form).attr('action'),
                data: $(form).serialize(),
                type: 'DELETE',
                success: function(data) {
                    data = JSON.parse(data);
                    if(data['status']) {
                        swal({
                            title: data['message'],
                            text: "Press OK to continue",
                            icon: "success",
                            buttons: {
                                cancel: true,
                                confirm: true,
                            },
                            closeOnConfirm: false,
                            closeOnEsc: false,
                        })
                        .then((willDelete) => {
                            if (willDelete) {
                                window.location.reload();
                            }
                            });
                        } else {
                             swal("Error", data['message'], "error");
                        }
                    }
                });
            } else {
                swal("Cancelled", title+" plan will not be deleted.", "error");
            }
        });
    }
</script>
@endsection