@extends('layouts.web')
@section('content')
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">User Management</h4>
            </div>
        </div>
    </div>
    <div class="container-fluid">
    	@include('flash::message')
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Users
	                        <div class="text-right">
                                <a class="btn btn-warning" href="{{ route('export') }}">Export User Data</a>
	                            <a href="{{ route('users.create') }}" type="button" class="btn btn-success" >Add</a>
	                        </div>
	                    </h5>
                        <div class="table-responsive">
                            <table id="zero_config" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>S.No.</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Phone Number</th>
                                        <th>Change Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                	@foreach($users as $key => $user)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>{{ $user->phone_number }}</td>
                                        <td>
                                        	<a href="{{ url('admin/users/blocked/'.$user->id) }}">
							                    @if($user->is_blocked)
							                    	<button type="button" class="btn-sm btn-danger" title="Unblock"><i class="fa fa-unlock"></i> Unblock</button>
							                	@else
							                		<button type="button" class="btn-sm btn-success" title="Block"><i class="fa fa-ban"></i> Block</button>
							            		@endif
	                                        </a>
		                                </td>
										<td class="">
											<div class="comment-footer">
	                                            <a href="{{ route('users.edit', $user->id) }}" type="button" class="btn btn-cyan btn-sm">Edit</a>
                                                <a href="{{ route('users.show', $user->id) }}" type="button" class="btn btn-info btn-sm">View</a>
    	                                        <form action="{{ route('users.destroy', $user->id) }}" method="POST" onsubmit="deleteUser('{{ $user->id }}', '{{ $user->name }}', event,this)">
                                                @csrf
                                                    <button class="btn-sm btn-danger" title="">DELETE</button>
                                                </form>
	                                        </div>
										</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>S.No.</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Phone Number</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer-script')
<script type="text/javascript">
    $(document).ready( function () {
        $('#zero_config').DataTable({
            "columnDefs": [{ 'orderable': false, 'targets': 5 }],
        });
    });

    function deleteUser(id, title, event,form)
    {
        event.preventDefault();
        swal({
            title: "Are you sure?",
            text: "You want to delete "+title+" user",
            icon: "warning",
            buttons: {
                cancel: true,
                confirm: true,
            },
            closeModal: false,
            closeModal: false,
            closeOnEsc: false,
        })
       .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                url: $(form).attr('action'),
                data: $(form).serialize(),
                type: 'DELETE',
                success: function(data) {
                    data = JSON.parse(data);
                    if(data['status']) {
                        swal({
                            title: data['message'],
                            text: "Press OK to continue",
                            icon: "success",
                            buttons: {
                                cancel: true,
                                confirm: true,
                            },
                            closeOnConfirm: false,
                            closeOnEsc: false,
                        })
                        .then((willDelete) => {
                            if (willDelete) {
                                window.location.reload();
                            }
                            });
                        } else {
                             swal("Error", data['message'], "error");
                        }
                    }
                });
            } else {
                swal("Cancelled", title+" user will not be deleted.", "error");
            }
        });
    }
</script>
@endsection
