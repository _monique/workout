@extends('layouts.web')

@section('top-css')
<style type="text/css">
img{
  max-width:180px;
  max-height:180px;
}
input[type=file]{
    padding:20px;
}
.page-breadcrumb .d-flex {
    justify-content: space-between;
}
</style>
@endsection

@section('content')
<div class="page-wrapper addUser">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Add User</h4>
                <a href="{{ route('users.index') }}" class="btn btn-primary float-right"><i class="fa fa-arrow-left"></i> Back</a>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <form class="form-horizontal" action="{{ route('users.store') }}" enctype="multipart/form-data" method="post">
            @csrf
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Add User</h4>
                            <div class="form-group row">
                                <label for="name" class="col-sm-3 text-right control-label col-form-label">Profile Image</label>
                                <div class="col-sm-9">
                                    <img id="blah" src="{{ asset('/admin/assets/images/users/1.jpg') }}" alt="Profile Image" />
                                    <br>
                                    <input type='file' name="profile_image" value="{{ old('profile_image') }}" onchange="readURL(this);" />
                                    <br>
                                    <span class="text-danger image_error">
                                        <strong>{{ $errors->has('profile_image')?$errors->first('profile_image'):'' }}</strong>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-sm-3 text-right control-label col-form-label">Full Name</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="name" value="{{ old('name') }}" id="name" placeholder="Full Name">
                                    <span class="text-danger image_error">
                                        <strong>{{ $errors->has('name')?$errors->first('name'):'' }}</strong>
                                    </span>
                                </div>   
                            </div>
                            <div class="form-group row">
                                <label for="email" class="col-sm-3 text-right control-label col-form-label">Email</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="email" id="email" value="{{ old('email') }}" placeholder="Email">
                                    <span class="text-danger image_error">
                                        <strong>{{ $errors->has('email')?$errors->first('email'):'' }}</strong>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="phone_number" class="col-sm-3 text-right control-label col-form-label">Phone No</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="phone_number" value="{{ old('phone_number') }}" id="phone_number" placeholder="Phone No">
                                    <span class="text-danger image_error">
                                        <strong>{{ $errors->has('phone_number')?$errors->first('phone_number'):'' }}</strong>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="password" class="col-sm-3 text-right control-label col-form-label">Password</label>
                                <div class="col-sm-9">
                                    <input type="password" class="form-control" value="{{ old('password') }}" name="password" id="password" placeholder="Password">
                                    <span class="text-danger image_error">
                                        <strong>{{ $errors->has('password')?$errors->first('password'):'' }}</strong>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="confirm" class="col-sm-3 text-right control-label col-form-label">Confirm Password</label>
                                <div class="col-sm-9">
                                    <input type="password" class="form-control" value="{{ old('password_confirmation') }}" name="password_confirmation" id="password_confirmation" placeholder="Confirm Password">
                                    <span class="text-danger image_error">
                                        <strong>{{ $errors->has('password_confirmation')?$errors->first('password_confirmation'):'' }}</strong>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="border-top">
                    <div class="card-body">
                        <button class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
@section('footer-script')
<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah')
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    
    var regex = RegExp(/^[0-9]{0,14}$/);
    $(document).on("keypress", "#phone_number", function (e) {
        
        if(regex.test($('#phone_number').val())){
            return true;
        }else{
            // event.preventDefault();
            return false;
        }
    });
</script>
@endsection