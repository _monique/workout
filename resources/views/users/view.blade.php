@extends('layouts.web')

@section('top-css')
<style type="text/css">
img{
  max-width:180px;
  max-height:180px;
}
.page-breadcrumb .d-flex {
    justify-content: space-between;
}
</style>
@endsection

@section('content')
<div class="page-wrapper viewUser">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">View User</h4>
                <a href="{{ route('users.index') }}" class="btn btn-primary float-right"><i class="fa fa-arrow-left"></i> Back</a>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">User Details</h4>
                        <div class = "col-md-12 row">
                            <div classs= "col-md-6">
                                <div class="row">
                                    <div class="col-sm-9">
                                        @if($user->profile_image == null)
                                        <img id="blah" src="{{ asset('/admin/assets/images/users/1.jpg') }}" alt="Profile Image" />
                                        @else
                                        <img id="blah" src="{{ asset('/images/'.$user->profile_image) }}" alt="Profile Image" />
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class ="col-md-6">
                                <div class="row">
                                    <label class="col-sm-6 text-right control-label col-form-label">Full Name : </label>
                                    <div class="col-sm-6">
                                        <label class="control-label col-form-label"> {{ $user->name }} </label>
                                    </div>   
                                </div>
                                <div class="row">
                                    <label for="email" class="col-sm-6 text-right control-label col-form-label">Email : </label>
                                    <div class="col-sm-6">
                                        <label class="control-label col-form-label"> {{ $user->email }}</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <label for="email" class="col-sm-6 text-right control-label col-form-label">Phone Number : </label>
                                    <div class="col-sm-6">
                                        <label class="control-label col-form-label"> {{ $user->phone_number }}</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <label for="email" class="col-sm-6 text-right control-label col-form-label">Height : </label>
                                    <div class="col-sm-6">
                                        <label class="control-label col-form-label"> {{ $user->height.' '.$user->height_unit }}</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <label for="email" class="col-sm-6 text-right control-label col-form-label">Weight : </label>
                                    <div class="col-sm-6">
                                        <label class="control-label col-form-label"> {{ $user->weight.' '.$user->weight_unit }}</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <label for="email" class="col-sm-6 text-right control-label col-form-label">Number of Child : </label>
                                    <div class="col-sm-6">
                                        <label class="control-label col-form-label"> {{ $user->no_of_child }}</label>
                                    </div>
                                </div>
                                <div class ="row">
                                    <label for="email" class="col-sm-6 text-right control-label col-form-label">Child Details : </label>
                                </div>
                                <div class="row">
                                    <label for="email" class="col-sm-6 text-right control-label col-form-label"> </label>
                                    <div class="col-sm-6">
										<table>
											<thead>
												<th>S.No.</th>
												<th> Name</th>
												<th> Height</th>
												<th> Weight</th>
												<th> Age </th>
											</thead>
											<tbody> 
												@foreach($user->childDetails()->get() as $key => $child)
												<tr>
													<td>{{ $key+1 }}</td>
													<td>{{ $child->child_name }}</td>
													<td>{{ $child->child_height.' '.$child->child_height_unit }}</td>
													<td>{{ $child->child_weight.' '.$child->child_weight_unit }}</td>
													<td>{{ $child->age->name }}</td>
												</tr>
												@endforeach
											</tbody>
										</table>
									</div>
                                </div>
                                <div class="row">
                                    <label for="email" class="col-sm-6 text-right control-label col-form-label">Health Goal : </label>
                                    <div class="col-sm-6">
                                        <label class="control-label col-form-label"> {{ isset($user->healthGoal->name) ? $user->healthGoal->name : '' }}</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <label for="email" class="col-sm-6 text-right control-label col-form-label">Subscription Plan Goal : </label>
                                    <div class="col-sm-6">
                                        <label class="control-label col-form-label"> 
                                            @if(!empty($user->subscriptionPlan))
                                                @foreach($user->subscriptionPlan as $plan) 
                                                    {{ $plan->name .' ('.$plan->validity.')' }} 
                                                @endforeach
                                            @endif</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection