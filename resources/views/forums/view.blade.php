@extends('layouts.web')

@section('top-css')
<style type="text/css">
img{
  max-width:180px;
  max-height:180px;
}
</style>
@endsection

@section('content')
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">View Forum Details</h4>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Forum Details</h4>
                        <div class = "col-md-12 row">
                            <div classs= "col-md-6">
                                <div class="row">
                                    <div class="col-sm-9">
                                        @if($forum->media == null)
                                        <img id="blah" src="{{ asset('/admin/assets/images/users/1.jpg') }}" alt="Media" />
                                        @else
                                        <img id="blah" src="{{ asset('/communityForums/'.$forum->media) }}" alt="Media" />
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class ="col-md-6">
                                <div class="row">
                                    <label class="col-sm-6 text-right control-label col-form-label">Forum Title : </label>
                                    <div class="col-sm-6">
                                        <label class="control-label col-form-label"> {{ $forum->title }} </label>
                                    </div>   
                                </div>
                                <div class="row">
                                    <label for="email" class="col-sm-6 text-right control-label col-form-label">Forum Description : </label>
                                    <div class="col-sm-6">
                                        <label class="control-label col-form-label"> {{ $forum->description }}</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <label for="email" class="col-sm-6 text-right control-label col-form-label">Total Likes : </label>
                                    <div class="col-sm-6">
                                        <label class="control-label col-form-label"> {{ $forum->totalLikes->count() }}</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <label for="email" class="col-sm-6 text-right control-label col-form-label">Total Comments : </label>
                                    <div class="col-sm-6">
                                        <label class="control-label col-form-label"> {{ $forum->comments->count() }}</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <label for="email" class="col-sm-6 text-right control-label col-form-label">Created By : </label>
                                    <div class="col-sm-6">
                                        <label class="control-label col-form-label"> {{ $forum->user->name }}</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <label for="email" class="col-sm-6 text-right control-label col-form-label">Created At : </label>
                                    <div class="col-sm-6">
                                        <label class="control-label col-form-label"> {{ $forum->created_at }}</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Forums Comments Details
                        </h5>
                        <div class="table-responsive">
                            <table id="zero_config" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>S.No.</th>
                                        <th>Commented By</th>
                                        <th>Comment</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($comments) > 0 )
                                    @foreach($comments as $key => $comment)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $comment->user->name }}</td>
                                        <td>{{ $comment->comment }}<br>
                                        <td class="">
                                            <div class="comment-footer">
                                                <form action="{{ route('delete-comment', $comment->id) }}" method="POST" onsubmit="deleteForumComment('{{ $comment->id }}', '{{ $forum->title }}', event,this)">
                                                @csrf
                                                    <button class="btn-sm btn-danger" title="">DELETE</button>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @else
                                        <tr>
                                        No comments found!
                                        </tr>
                                    @endif
                                    
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>S.No.</th>
                                        <th>Title</th>
                                        <th>Reported By</th>
                                        <th>Posted By</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('footer-script')
<script type="text/javascript">
    $(document).ready( function () {
        $('#data-table').DataTable();
    });

    function deleteForumComment(id, title, event,form)
    {
        event.preventDefault();
        swal({
            title: "Are you sure?",
            text: "You want to delete "+title+" forum comment",
            icon: "warning",
            buttons: {
                cancel: true,
                confirm: true,
            },
            closeModal: false,
            closeModal: false,
            closeOnEsc: false,
        })
       .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                url: $(form).attr('action'),
                data: $(form).serialize(),
                method: 'POST',
                success: function(data) {
                    data = JSON.parse(data);
                    if(data['status']) {
                        swal({
                            title: data['message'],
                            text: "Press OK to continue",
                            icon: "success",
                            buttons: {
                                cancel: true,
                                confirm: true,
                            },
                            closeOnConfirm: false,
                            closeOnEsc: false,
                        })
                        .then((willDelete) => {
                            if (willDelete) {
                                window.location.reload();
                            }
                            });
                        } else {
                             swal("Error", data['message'], "error");
                        }
                    }
                });
            } else {
                swal("Cancelled", title+" forum comment will not be deleted.", "error");
            }
        });
    }
</script>
@endsection
