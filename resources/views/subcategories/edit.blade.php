@extends('layouts.web')

@section('top-css')
<style type="text/css">
img{
  max-width:180px;
  max-height:180px;
}
input[type=file]{
    padding:20px;
}
</style>
@endsection

@section('content')
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Edit Sub Category</h4>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <form class="form-horizontal" action="{{ route('subcategories.update' , $subcategory->id) }}" enctype="multipart/form-data" method="post">
            @csrf
            @method('put')
            <div class="card row">
                <div class="col-md-12">
                    <div class="card-body">
                        <h4 class="card-title">Edit Category</h4>
                        <div class="form-group row">
                            <label for="image" class="col-sm-3 text-right control-label col-form-label">Image</label>
                            <div class="col-sm-9">
                                @if($subcategory->image == null)
                                <img id="blah" src="{{ asset('/categories/default.png') }}" alt="Image" />
                                @else
                                <img id="blah" src="{{ asset('/categories/'.$subcategory->image) }}" alt="Image" />
                                @endif
                                <br>
                                <input type='file' name="image" value="{{ old('image') }}" onchange="readURL(this);" />
                                <br>
                                <span class="text-danger image_error">
                                    <strong>{{ $errors->has('image')?$errors->first('image'):'' }}</strong>
                                </span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-sm-3 text-right control-label col-form-label">Category</label>
                            <div class="col-sm-9">
                                <select type="text" class="form-control" name="category_id" id="category_id" placeholder="-Select Category-" >
                                    @foreach($categories as $category)
                                    <option value="{{ $category['id'] }}" {{ old('category_id', $subcategory->category_id) == $category['id'] ? 'selected' : '' }} ">{{ $category['name'] }}</option>
                                    @endforeach
                                </select>    
                                <span class="text-danger image_error">
                                    <strong>{{ $errors->has('category_id')?$errors->first('category_id'):'' }}</strong>
                                </span>
                            </div>   
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-sm-3 text-right control-label col-form-label">Name</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="{{ old('name', $subcategory->name) }}">
                                <span class="text-danger image_error">
                                    <strong>{{ $errors->has('name')?$errors->first('name'):'' }}</strong>
                                </span>
                            </div>   
                        </div>
                         <div class="form-group row">
                            <label for="name" class="col-sm-3 text-right control-label col-form-label">Description</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="description" id="description" placeholder="description" value="{{ old('description', $subcategory->description) }}">
                                <span class="text-danger image_error">
                                    <strong>{{ $errors->has('description')?$errors->first('description'):'' }}</strong>
                                </span>
                            </div>   
                        </div>
                    </div>
                </div>
                <div class="border-top">
                    <div class="card-body">
                        <button class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@section('footer-script')
<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah')
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
@endsection