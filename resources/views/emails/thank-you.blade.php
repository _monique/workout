<!DOCTYPE html>
<html lang="en">
<head>
  <title></title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
  <style>
	.thank-you-content-wrap{
		padding: 0 20px;
	}
	.thank-you-content-wrap:before {
		content: '';
		width: 100vw;
		height: 100vh;
		position: fixed;
		top: 0;
		left: 0;
		background: linear-gradient(to top, #9e89e6, #9e89e6, #86b2ef, #86b2ef);
		z-index: -1;
	}
	.thank-you-content {
		background: #fff;
		border-radius: 10px;
		margin: 30px auto 50px;
		padding: 15px 25px;
		max-width: 500px;
		min-width: 500px;
		box-shadow: 0 0 5px #efefef;
		text-align: center;
	}
	.success-check{
	    font-size: 50px;
		color: #058c05;
	}
	h2{
		font-weight: 100;
		margin-bottom: 0;
	}
	h1{
		margin-top: 10px;
	}
	img{
		width: 130px;box-shadow: 0px 0px 20px #dcdcdc;border-radius: 50%;
	}
	p{
		text-decoration: none;background: linear-gradient(to top, #9e89e6, #86b2ef);color: #fff;font-size: 20px;padding: 7px 15px;display: block;border-radius: 5px;margin: 5px 0;width: 156px;margin: 20px auto 10px;
	}
	</style>
</head>
<body>
<div class="thank-you-content-wrap">
	<div class="container thank-you-content">
		<i class="fas fa-check-circle success-check"></i>
		<h2>Thank You for Verifing</h2>
		<h1>Welcome to Workout</h1>
		<img src="{{asset('images/logo.png')}}" >
		<p>Please login via App</p>
	</div>
</div>
</body>
</html>
