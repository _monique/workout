@extends('layouts.web')

@section('top-css')
<style type="text/css">
.page-breadcrumb .d-flex {
    justify-content: space-between;
}
</style>
@endsection

@section('content')
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Add subscription Plan</h4>
                <a href="{{ route('subscriptions.index') }}" class="btn btn-primary float-right"><i class="fa fa-arrow-left"></i> Back</a>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <form class="form-horizontal" action="{{ route('subscriptions.store') }}" enctype="multipart/form-data" method="post">
            @csrf
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Add subscription Plan</h4>
                            <div class="form-group row">
                                <label for="plan_title" class="col-sm-3 text-right control-label col-form-label">Plan Title</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="plan_title" value="{{ old('plan_title') }}" id="plan_title" placeholder="Plan Title">
                                    <span class="text-danger image_error">
                                        <strong>{{ $errors->has('plan_title')?$errors->first('plan_title'):'' }}</strong>
                                    </span>
                                </div>   
                            </div>
                            <div class="form-group row">
                                <label for="plan_price" class="col-sm-3 text-right control-label col-form-label">Plan Price ($)</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="plan_price" id="plan_price" value="{{ old('plan_price') }}" placeholder="Plan Price">
                                    <span class="text-danger image_error">
                                        <strong>{{ $errors->has('plan_price')?$errors->first('plan_price'):'' }}</strong>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="plan_period" class="col-sm-3 text-right control-label col-form-label">Plan Period</label>
                                <div class="col-md-4">
                                    <select class="select2 form-control custom-select select2-hidden-accessible" style="width: 100%; height:36px;" data-select2-id="1" tabindex="-1" aria-hidden="true" name="plan_time">
                                        <option value="">Select</option>
                                        <option value="1" {{ old('plan_time') == 1 ? 'selected' : '' }}>1</option>
                                        <option value="2" {{ old('plan_time') == 2 ? 'selected' : '' }}>2</option>
                                        <option value="3" {{ old('plan_time') == 3 ? 'selected' : '' }}>3</option>
                                        <option value="4" {{ old('plan_time') == 4 ? 'selected' : '' }}>4</option>
                                        <option value="5" {{ old('plan_time') == 5 ? 'selected' : '' }}>5</option>
                                        <option value="6" {{ old('plan_time') == 6 ? 'selected' : '' }}>6</option>
                                        <option value="7" {{ old('plan_time') == 7 ? 'selected' : '' }}>7</option>
                                        <option value="8" {{ old('plan_time') == 8 ? 'selected' : '' }}>8</option>
                                        <option value="9" {{ old('plan_time') == 9 ? 'selected' : '' }}>9</option>
                                        <option value="10" {{ old('plan_time') == 10 ? 'selected' : '' }}>10</option>
                                        <option value="11" {{ old('plan_time') == 11 ? 'selected' : '' }}>11</option>
                                        <option value="12" {{ old('plan_time') == 12 ? 'selected' : '' }}>12</option>
                                    </select>
                                    <span class="text-danger image_error">
                                        <strong>{{ $errors->has('plan_time')?$errors->first('plan_time'):'' }}</strong>
                                    </span>
                                </div>
                                <div class="col-md-5">
                                    <select class="select2 form-control custom-select select2-hidden-accessible" style="width: 100%; height:36px;" data-select2-id="1" tabindex="-1" aria-hidden="true" name="plan_period">
                                        <option value="">Select</option>
                                        <option value="month" {{ old('plan_period') == 'month' ? 'selected' : '' }}>Months</option>
                                        <option value="year" {{ old('plan_period') == 'year' ? 'selected' : '' }}>Years</option>
                                    </select>
                                    <span class="text-danger image_error">
                                        <strong>{{ $errors->has('plan_period')?$errors->first('plan_period'):'' }}</strong>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="plan_details" class="col-sm-3 text-right control-label col-form-label">Plan Details**</label>
                                <div class="col-sm-9" data-container="body" data-toggle="popover" data-placement="top" data-content="Eg.: Personal Trainer, 30 Classes, Massage & Sauna">
                                    <input type="text" class="form-control" value="{{ old('plan_details') }}" name="plan_details" id="plan_details" placeholder="Plan Details">
                                    <span class="text-danger image_error">
                                        <strong>{{ $errors->has('plan_details')?$errors->first('plan_details'):'' }}</strong>
                                    </span><br>
                                    <div class="">Note** : Please enter comma separated Plan Details.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="border-top">
                    <div class="card-body">
                        <button class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
@section('footer-script')
<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah')
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
@endsection