<!DOCTYPE html>
<html lang="en">
<head>
  <title></title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
  <style>
	.thank-you-content-wrap{
		padding: 0 20px;
	}
	.thank-you-content-wrap:before {
		content: '';
		width: 100vw;
		height: 100vh;
		position: fixed;
		top: 0;
		left: 0;
		background: linear-gradient(to top, #9e89e6, #9e89e6, #86b2ef, #86b2ef);
		z-index: -1;
	}
	.thank-you-content-wrap .thank-you-content {
		background: #fff;
		border-radius: 10px;
		margin: 30px auto 50px;
		padding: 15px 25px;
		max-width: 500px;
		box-shadow: 0 0 5px #efefef;
		text-align: center;
	}
	.thank-you-content-wrap .success-check{
	    font-size: 50px;
		color: #058c05;
	}
	.thank-you-content-wrap h2{
		font-weight: 100;
		margin-bottom: 0;
	}
	.thank-you-content-wrap h1{
		margin-top: 10px;
	}
	.thank-you-content-wrap img{
		width: 130px;box-shadow: 0px 0px 20px #dcdcdc;border-radius: 50%;
	}
	.thank-you-content-wrap p{
		text-decoration: none;background: linear-gradient(to top, #9e89e6, #86b2ef);color: #fff;font-size: 20px;padding: 7px 15px;display: block;border-radius: 5px;margin: 5px 0;width: 178px;margin: 20px auto 10px;
	}
	</style>
</head>
<body>
<div class="thank-you-content-wrap">
	<div class="container thank-you-content">
		<i class="fas fa-check-circle success-check"></i>
		<h2>Thank You for verifying </h2>
		<h1>Welcome to moFITMOM<sup>tm</sup></h1>
		<img src="{{ asset('images/logo.png') }}" >
		<h2>Please login via App</h2>
	</div>
</div>
</body>
</html>
