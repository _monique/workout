@extends('layouts.web')

@section('top-css')
 <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<script type="text/javascript" src="https://momentjs.com/downloads/moment-with-locales.min.js"></script>
<link rel="stylesheet" href="{{ asset('admin/assets1/material_datetimepicker/css/bootstrap-material-datetimepicker.css') }}" />
<script type="text/javascript" src="{{ asset('admin/assets1/material_datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>
@endsection
@section('content')
<div class="page-wrapper">
     <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Dashboard</h4>
            </div>
        </div>
    </div>
        @include('flash::message')
    <div class="container-fluid">
        <div class="row">
            <!-- Column -->
            <div class="col-md-6 col-lg-2 col-xlg-3">
                <a href = "{{ route('users.index') }}">
                    <div class="card card-hover">
                        <div class="box bg-cyan text-center">
                            <h1 class="font-light text-white"><i class="mdi mdi-account-multiple"></i></h1>
                            <h6 class="text-white">Users</h6>
                        </div>
                    </div>
                 </a>
            </div>
            <!-- Column -->
            <div class="col-md-6 col-lg-2 col-xlg-3">
                <a href = "{{ route('workoutPlans.index') }}">
                    <div class="card card-hover">
                        <div class="box bg-danger text-center">
                            <h1 class="font-light text-white"><i class="mdi mdi-file-document"></i></h1>
                            <h6 class="text-white">WorkOut Plans</h6>
                        </div>
                    </div>
                </a>
            </div>
            <!-- Column -->
            <div class="col-md-6 col-lg-2 col-xlg-3">
                <a href = "{{ route('subscriptions.index') }}">
                    <div class="card card-hover">
                        <div class="box bg-info text-center">
                            <h1 class="font-light text-white"><i class="far fa-list-alt"></i></h1>
                            <h6 class="text-white">Subscription Plans</h6>
                        </div>
                    </div>
                </a>
            </div>
            <!-- Column -->
            <div class="col-md-6 col-lg-2 col-xlg-3">
                <a href = "{{ route('contactUs.index') }}">
                    <div class="card card-hover">
                        <div class="box bg-cyan text-center">
                            <h1 class="font-light text-white"><i class="mdi mdi-email-alert"></i></h1>
                            <h6 class="text-white">Contact Us</h6>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- Sales chart -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="d-md-flex align-items-center">
                            <div>
                                <h4 class="card-title">Site Analysis</h4>
                                <h5 class="card-subtitle">Overview of Latest Year</h5>
                            </div>
                        </div>
                        <div class="row">
                          <div class="selectdate col-md-6">
                            <form method="GET">
                              <input type="text" id="start_date" class="form-control floating-label" placeholder="Start Date" style="max-width: 120px;float: left; margin-right: 30px" name="start_date" value="{{  old('start_date', Session::get('start_date')) }}" >
                              <span style="display: inline;float: left;margin-right: 30px;">To </span>
                              <input type="text" id="end_date" class="form-control floating-label" placeholder="End Date" style="max-width: 120px;float: left; margin-right: 30px" name="end_date" value="{{ old('end_date', Session::get('end_date')) }}" >
                              <input type="submit" value="Search">
                              
                            </form>
                            <script>
                              $('#start_date').bootstrapMaterialDatePicker({
                                  format : 'MM/DD/YYYY',
                                  //weekStart : 0, 
                                  time: false ,
                                  //minDate : new Date(),
                              }).on('change', function(e, date){ 
                                  $('#end_date').bootstrapMaterialDatePicker('setMinDate', date);
                               });

                              $('#end_date').bootstrapMaterialDatePicker({
                                  format : 'MM/DD/YYYY',
                                  //weekStart : 0, 
                                  time: false ,
                                  maxDate : new Date(),
                              }).on('change', function(e, date){  });
                            </script>
                          </div>
                           <div class="col-md-6">
                              <a href="{{ route('dashboard') }}" onclick="ClearFields();"><button class="btn btn-info">Refresh</button></a>
                            </div>
                        </div>
                        <div class="row">
                            <!-- column -->
                            <div class="col-lg-10">
                                <div class="chart-container">
                                    <div class="row pie-chart-container">
                                        <div class="col-lg-6">
                                            <canvas id="pie-chart-user"></canvas>
                                        </div>
                                        <div class="col-lg-6">
                                            <canvas id="pie-chart-workout"></canvas>
                                        </div>
                                        <div class="col-lg-6">
                                            <canvas id="pie-chart-contactUs"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="bg-dark p-10 text-white text-center">
                                           <i class="fa fa-user m-b-5 font-16"></i>
                                           <h5 class="m-b-0 m-t-5">{{ $totalUsers }}</h5>
                                           <small class="font-light">Total Users</small>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="bg-dark p-10 text-white text-center">
                                           <i class="fa fa-user m-b-5 font-16"></i>
                                           <h5 class="m-b-0 m-t-5">{{ $newUsers }}</h5>
                                           <small class="font-light">New Users</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                            <!-- column -->
                            <div class="col-lg-12">
                                <div class="chart-container">
                                    <div class="row pie-chart-container">
                                        <div class="col-lg-12">
                                            <canvas id="pie-chart-contactUs"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- column -->
                        </div>
                            <!-- column -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('footer-script')

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script type="text/javascript">
$(function(){

  function ClearFields() {
    alert('hello');
     document.getElementById("start_date").value = "";
     document.getElementById("end_date").value = "";
  }

  //get the pie chart canvas
  var cData = JSON.parse(`<?php echo $chartData['user_data']; ?>`);
  var ctx = $("#pie-chart-user");

  //pie chart data
  var data = {
    labels: cData.label,
    datasets: [
      {
        label: "Users Count",
        data: cData.data,
        backgroundColor: [
          "#DEB887",
          "#A9A9A9",
          "#DC143C",
          "#F4A460",
          "#2E8B57",
          "#1D7A46",
          "#CDA776",
        ],
        borderColor: [
          "#CDA776",
          "#989898",
          "#CB252B",
          "#E39371",
          "#1D7A46",
          "#F4A460",
          "#CDA776",
        ],
        borderWidth: [1, 1, 1, 1, 1,1,1]
      }
    ]
  };

  //options
  var options = {
    responsive: true,
    title: {
      display: true,
      position: "top",
      text: "Registered Users -  Month Wise Count",
      fontSize: 18,
      fontColor: "#111"
    },
    legend: {
      display: true,
      position: "bottom",
      labels: {
        fontColor: "#333",
        fontSize: 16
      }
    }
  };

  //create Pie Chart class object
  var chart1 = new Chart(ctx, {
    type: "pie",
    data: data,
    options: options
  });
});
  
$(function(){
  //get the pie chart canvas
  var cData = JSON.parse(`<?php echo $chartData['workout_data']; ?>`);
  var ctx = $("#pie-chart-workout");

  //pie chart data
  var data = {
    labels: cData.label,
    datasets: [
      {
        label: "Workout Plans Count",
        data: cData.data,
        backgroundColor: [
          "#DEB887",
          "#A9A9A9",
          "#DC143C",
          "#F4A460",
          "#2E8B57",
          "#1D7A46",
          "#CDA776",
        ],
        borderColor: [
          "#CDA776",
          "#989898",
          "#CB252B",
          "#E39371",
          "#1D7A46",
          "#F4A460",
          "#CDA776",
        ],
        borderWidth: [1, 1, 1, 1, 1,1,1]
      }
    ]
  };

  //options
  var options = {
    responsive: true,
    title: {
      display: true,
      position: "top",
      text: "Workout Plans -  Month Wise Count",
      fontSize: 18,
      fontColor: "#111"
    },
    legend: {
      display: true,
      position: "bottom",
      labels: {
        fontColor: "#333",
        fontSize: 16
      }
    }
  };

  //create Pie Chart class object
  var chart1 = new Chart(ctx, {
    type: "pie",
    data: data,
    options: options
  });
});
  
$(function(){
  //get the pie chart canvas
  var cData = JSON.parse(`<?php echo $chartData['contactUs']; ?>`);
  var ctx = $("#pie-chart-contactUs");

  //pie chart data
  var data = {
    labels: cData.label,
    datasets: [
      {
        label: "Users Contacted Count",
        data: cData.data,
        backgroundColor: [
          "#DEB887",
          "#A9A9A9",
          "#DC143C",
          "#F4A460",
          "#2E8B57",
          "#1D7A46",
          "#CDA776",
        ],
        borderColor: [
          "#CDA776",
          "#989898",
          "#CB252B",
          "#E39371",
          "#1D7A46",
          "#F4A460",
          "#CDA776",
        ],
        borderWidth: [1, 1, 1, 1, 1,1,1]
      }
    ]
  };

  //options
  var options = {
    responsive: true,
    title: {
      display: true,
      position: "top",
      text: "Contacted Users -  Month Wise Count",
      fontSize: 18,
      fontColor: "#111"
    },
    legend: {
      display: true,
      position: "bottom",
      labels: {
        fontColor: "#333",
        fontSize: 16
      }
    }
  };

  //create Pie Chart class object
  var chart1 = new Chart(ctx, {
    type: "pie",
    data: data,
    options: options
  });
});
</script>

@endsection
