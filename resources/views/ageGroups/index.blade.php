@extends('layouts.web')

@section('content')
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Age Group Management</h4>
            </div>
        </div>
    </div>
    <div class="container-fluid">
    	@include('flash::message')
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Age Group
	                        <div class="text-right">
	                            <a href="{{ route('ageGroups.create') }}" type="button" class="btn btn-success" >Add</a>
	                        </div>
	                    </h5>
                        <div class="table-responsive">
                            <table id="zero_config" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>S.No.</th>
                                        <th>Title</th>
                                        <th>Created At</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                	@foreach($ageGroups as $key => $ageGroup)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $ageGroup->name }}</td>
                                        <td>{{ $ageGroup->created_at->format('d M Y') }}</td> 
										<td class="">
											<div class="comment-footer">
	                                            <a href="{{ route('ageGroups.edit', $ageGroup->id) }}" type="button" class="btn btn-cyan btn-sm">Edit</a>
    	                                        <form action="{{ route('ageGroups.destroy', $ageGroup->id) }}" method="POST" onsubmit="deleteAgeGroup('{{ $ageGroup->id }}', '{{ $ageGroup->name }}', event,this)">
                                                @csrf
                                                    <button class="btn-sm btn-danger" title="">DELETE</button>
                                                </form>
	                                        </div>
										</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>S.No.</th>
                                        <th>Name</th>
                                        <th>Created At</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer-script')
<script type="text/javascript">
    $(document).ready( function () {
        $('#zero_config').DataTable({
            "columnDefs": [{ 'orderable': false, 'targets': 3 }],
        });
    });

    function deleteAgeGroup(id, title, event,form)
    {
        event.preventDefault();
        swal({
            title: "Are you sure?",
            text: "You want to delete "+title+" age group",
            icon: "warning",
            buttons: {
                cancel: true,
                confirm: true,
            },
            closeModal: false,
            closeModal: false,
            closeOnEsc: false,
        })
       .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                url: $(form).attr('action'),
                data: $(form).serialize(),
                type: 'DELETE',
                success: function(data) {
                    data = JSON.parse(data);
                    if(data['status']) {
                        swal({
                            title: data['message'],
                            text: "Press OK to continue",
                            icon: "success",
                            buttons: {
                                cancel: true,
                                confirm: true,
                            },
                            closeOnConfirm: false,
                            closeOnEsc: false,
                        })
                        .then((willDelete) => {
                            if (willDelete) {
                                window.location.reload();
                            }
                            });
                        } else {
                             swal("Error", data['message'], "error");
                        }
                    }
                });
            } else {
                swal("Cancelled", title+" age group will not be deleted.", "error");
            }
        });
    }
</script>
@endsection
