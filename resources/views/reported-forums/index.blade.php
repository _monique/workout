@extends('layouts.web')
@section('content')
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Reported Community Forums</h4>
            </div>
        </div>
    </div>
    <div class="container-fluid">
    	@include('flash::message')
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Reported Forums
	                    </h5>
                        <div class="table-responsive">
                            <table id="zero_config" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>S.No.</th>
                                        <th>Title</th>
                                        <th>Reported By</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                	@foreach($forums as $key => $forum)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $forum->forum->title }} <br>  
                                            <strong>Posted By</strong> -  {{ $forum->forum->user->name }}</td>
                                        <td>{{ $forum->user->name }}<br>
                                            <strong>Reason - </strong> {{ $forum->reason }}</td>
                                        <td class="">
                                            <div class="comment-footer">
                                                <a href="{{ route('reported-forums.show', $forum->id) }}" type="button" class="btn btn-info btn-sm">View</a>
                                                <form action="{{ route('reported-forums.destroy', $forum->id) }}" method="POST" onsubmit="deleteForumReport('{{ $forum->id }}', '{{ $forum->title }}', event,this)">
                                                @csrf
                                                    <button class="btn-sm btn-danger" title="">DELETE</button>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>S.No.</th>
                                        <th>Title</th>
                                        <th>Reported By</th>
                                        <th>Posted By</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer-script')
<script type="text/javascript">
    $(document).ready( function () {
       $('#zero_config').DataTable({
            "columnDefs": [{ 'orderable': false, 'targets': 5 }],
        });
    });

    function deleteForumReport(id, title, event,form)
    {
        event.preventDefault();
        swal({
            title: "Are you sure?",
            text: "You want to delete "+title+" forum report",
            icon: "warning",
            buttons: {
                cancel: true,
                confirm: true,
            },
            closeModal: false,
            closeModal: false,
            closeOnEsc: false,
        })
       .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                url: $(form).attr('action'),
                data: $(form).serialize(),
                method: 'DELETE',
                success: function(data) {
                    data = JSON.parse(data);
                    if(data['status']) {
                        swal({
                            title: data['message'],
                            text: "Press OK to continue",
                            icon: "success",
                            buttons: {
                                cancel: true,
                                confirm: true,
                            },
                            closeOnConfirm: false,
                            closeOnEsc: false,
                        })
                        .then((willDelete) => {
                            if (willDelete) {
                                window.location.reload();
                            }
                            });
                        } else {
                             swal("Error", data['message'], "error");
                        }
                    }
                });
            } else {
                swal("Cancelled", title+" forum report will not be deleted.", "error");
            }
        });
    }
</script>
@endsection
