@extends('layouts.web')

@section('top-css')
<style type="text/css">
img{
  max-width:180px;
  max-height:180px;
}
</style>
@endsection

@section('content')
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">View Forum Details</h4>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Forum Details</h4>
                        <div class = "col-md-12 row">
                            <div classs= "col-md-6">
                                <div class="row">
                                    <div class="col-sm-9">
                                        @if($report->forum->media == null)
                                        <img id="blah" src="{{ asset('/admin/assets/images/users/1.jpg') }}" alt="Media" />
                                        @else
                                        <img id="blah" src="{{ asset('/communityForums/'.$report->forum->media) }}" alt="Media" />
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class ="col-md-6">
                                <div class="row">
                                    <label class="col-sm-6 text-right control-label col-form-label">Forum Title : </label>
                                    <div class="col-sm-6">
                                        <label class="control-label col-form-label"> {{ $report->forum->title }} </label>
                                    </div>   
                                </div>
                                <div class="row">
                                    <label for="email" class="col-sm-6 text-right control-label col-form-label">Forum Description : </label>
                                    <div class="col-sm-6">
                                        <label class="control-label col-form-label"> {{ $report->forum->description }}</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <label for="email" class="col-sm-6 text-right control-label col-form-label">Total Likes : </label>
                                    <div class="col-sm-6">
                                        <label class="control-label col-form-label"> {{ $report->forum->totalLikes->count() }}</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <label for="email" class="col-sm-6 text-right control-label col-form-label">Total Comments : </label>
                                    <div class="col-sm-6">
                                        <label class="control-label col-form-label"> {{ $report->forum->comments->count() }}</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <label for="email" class="col-sm-6 text-right control-label col-form-label">Created By : </label>
                                    <div class="col-sm-6">
                                        <label class="control-label col-form-label"> {{ $report->forum->user->name }}</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <label for="email" class="col-sm-6 text-right control-label col-form-label">Created At : </label>
                                    <div class="col-sm-6">
                                        <label class="control-label col-form-label"> {{ $report->forum->created_at }}</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Reported Details:</h4>
                        <div class = "col-md-12 row">
                            <div class ="col-md-6">
                                <div class="row">
                                    <label class="col-sm-6 text-right control-label col-form-label">Reported By : </label>
                                    <div class="col-sm-6">
                                        <label class="control-label col-form-label"> {{ $report->user->name }} </label>
                                    </div>   
                                </div>
                                <div class="row">
                                    <label for="email" class="col-sm-6 text-right control-label col-form-label">Reason : </label>
                                    <div class="col-sm-6">
                                        <label class="control-label col-form-label"> {{ $report->reason }}</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection