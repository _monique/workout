<style>
body {
    min-height: 100vh;
    position: relative;
    background: linear-gradient(to top, #9e89e6, #86b2ef);
}
.reset-pass-page > .row {
    height: calc(100vh - 90px);
}
.logo-wrapper img {
    max-width: 180px;
    height: 180px;
    margin-bottom: 15px;
}
.card {
    box-shadow: 0 0 6px 0px #e4e4e4;
}
.reset-pass-page .card-header {
    background: #fff;
    text-align: center;
    font-size: 20px;
}
</style>

@extends('layouts.app')

@section('content')
<div class="container reset-pass-page">
    <div class="row justify-content-center align-items-center">
        <div class="col-md-5">
            <div class="card">
                <div class="card-header">{{ __('Reset Password') }}</div>

                <div class="card-body">
					<div class="text-center p-t-20 p-b-20 logo-wrapper">
						<span class="db"><a href=""><img src="http://workout.projectdevelopment.co/public/images/logo.png" height="50%" width="50%" alt="logo"></a></span>
					</div>
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group row">
                            

                            <div class="col-md-12">
                                <input id="email" type="email" class="form-control form-control-lg @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="Email" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-success float-right">
                                    {{ __('Send Password Reset Link') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
