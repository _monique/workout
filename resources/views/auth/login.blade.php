@extends('layouts.web')

@section('content') 
<div class="auth-wrapper d-flex no-block justify-content-center align-items-center bg-dark">
    <div class="auth-box bg-dark border-top">
        <div id="loginform">
            <div class="text-center p-t-20 p-b-20">
                <span class="db"><a href=""><img src="{{ asset('images/logo.png') }}" height="50%" width="50%" alt="logo" /></a></span>
            </div>
            <!-- Form -->
            <form class="form-horizontal m-t-20" id="loginform" method="post" action="{{ route('login') }}">
                @csrf
                <div class="row p-b-30">
                    <div class="col-12">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text bg-success text-white" id="basic-addon1"><i class="ti-email"></i></span>
                            </div>

                            <input id="email" type="email" class="form-control form-control-lg @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="Email" required autocomplete="email" autofocus>

                           
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text bg-warning text-white" id="basic-addon2"><i class="ti-pencil"></i></span>
                            </div>
                            
                            <input id="password" type="password" class="form-control form-control-lg @error('email') is-invalid @enderror" name="password" placeholder="Password" required autocomplete="current-password">

                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row border-top border-secondary">
                    <div class="col-12">
                        <div class="form-group">
                            <div class="p-t-20">
                                <a href="{{ route('password.request') }}"><button class="btn btn-info" id="to-recover" type="button"><i class="fa fa-lock m-r-5"></i> Lost password?</button></a>
                                <button class="btn btn-success float-right" type="submit">Login</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
