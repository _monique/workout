@extends('layouts.web')

@section('content')
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Change Password</h4>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        @include('flash::message')
        <form class="form-horizontal" method="post" action="{{ route('update.password') }}" >
            @csrf
            @method('put')
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Change Password</h4>
                            <div class="form-group row">
                                <label for="current_password" class="col-sm-3 text-right control-label col-form-label">Current Password</label>
                                <div class="col-sm-9">
                                    <input type="password" class="form-control" name="current_password" value="{{ old('current_password') }}" id="current_password" placeholder="Current Password">
                                    <span class="ptxt" style="cursor:pointer;float:right;color:#002e6d;" onclick="changepassword('current_password', 'ptxt1')" id="ptxt1">Show</span>
                                    <span class="text-danger image_error">
                                        <strong>{{ $errors->has('current_password')?$errors->first('current_password'):'' }}</strong>
                                    </span>
                                </div>   
                            </div>
                            <div class="form-group row">
                                <label for="password" class="col-sm-3 text-right control-label col-form-label">New Password</label>
                                <div class="col-sm-9">
                                    <input type="password" class="form-control" name="password" id="password" value="{{ old('password') }}" placeholder="New Password">
                                    <span class="ptxt" style="cursor:pointer;float:right;color:#002e6d;" onclick="changepassword('password', 'ptxt2')" id="ptxt2">Show</span>
                                    <span class="text-danger image_error">
                                        <strong>{{ $errors->has('password')?$errors->first('password'):'' }}</strong>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="password_confirmation" class="col-sm-3 text-right control-label col-form-label">Confirm Password</label>
                                <div class="col-sm-9">
                                    <input type="password" class="form-control" value="{{ old('password_confirmation') }}" name="password_confirmation" id="password_confirmation" placeholder="Confirm Password">
                                    <span class="ptxt" style="cursor:pointer;float:right;color:#002e6d;" onclick="changepassword('password_confirmation', 'ptxt3')" id="ptxt3">Show</span>   
                                    <span class="text-danger image_error">
                                        <strong>{{ $errors->has('password_confirmation')?$errors->first('password_confirmation'):'' }}</strong>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="border-top">
                    <div class="card-body">
                        <button class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
@section('footer-script')
<script type="text/javascript">
    function changepassword(inputid, selfid){
        $("#"+selfid).text($("#"+selfid).text() == "Show" ? "Hide" : "Show"); 
        $("#"+inputid).attr('type', function(index, attr){return attr == 'password' ? 'text' : 'password'; }); 
    }  
</script>
@endsection