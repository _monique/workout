@extends('layouts.web')

@section('top-css')
<style type="text/css">
img{
  max-width:180px;
}
input[type=file]{
    padding:20px;
}
</style>
@endsection

@section('content')

<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">My Profile</h4>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        @include('flash::message')
        <form class="form-horizontal" action="{{ route('update.profile' , $user->id) }}" enctype="multipart/form-data" method="post">
            @csrf
            @method('put')
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Edit Profile</h4>
                            <div class="form-group row">
                                <label for="name" class="col-sm-3 text-right control-label col-form-label">Profile Image</label>
                                <div class="col-sm-9">
                                    @if($user->profile_image == null)
                                    <img id="blah" src="{{ asset('/admin/assets/images/users/1.jpg') }}" alt="Profile Image" />
                                    @else
                                    <img id="blah" src="{{ asset('/images/'.$user->profile_image) }}" alt="Profile Image" />
                                    @endif
                                    <br>
                                    <input type='file' name="profile_image" value="{{ old('profile_image') }}" onchange="readURL(this);" />
                                    <br>
                                    <span class="text-danger image_error">
                                        <strong>{{ $errors->has('profile_image')?$errors->first('profile_image'):'' }}</strong>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-sm-3 text-right control-label col-form-label">Full Name</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="name" id="name" placeholder="Full Name" value="{{ old('name', $user->name) }}">
                                    <span class="text-danger image_error">
                                        <strong>{{ $errors->has('name')?$errors->first('name'):'' }}</strong>
                                    </span>
                                </div>   
                            </div>
                            <div class="form-group row">
                                <label for="email" class="col-sm-3 text-right control-label col-form-label">Email</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="email" id="email" placeholder="Email" value="{{ old('email', $user->email) }}">
                                    <span class="text-danger image_error">
                                        <strong>{{ $errors->has('email')?$errors->first('email'):'' }}</strong>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="phone_number" class="col-sm-3 text-right control-label col-form-label">Phone No</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="phone_number" id="phone_number" placeholder="Phone No" value="{{ old('phone_number', $user->phone_number) }}">
                                    <span class="text-danger image_error">
                                        <strong>{{ $errors->has('phone_number')?$errors->first('phone_number'):'' }}</strong>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="border-top">
                    <div class="card-body">
                        <button class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
@section('footer-script')
<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah')
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
@endsection