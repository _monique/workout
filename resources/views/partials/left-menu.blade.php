<aside class="left-sidebar" data-sidebarbg="skin5">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav" class="p-t-30">
                <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ route('dashboard') }}" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu">Dashboard</span></a></li>

                <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ route('users.index') }}" aria-expanded="false"><i class="mdi mdi-account-multiple"></i><span class="hide-menu">User Management</span></a></li>

                <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ route('workoutPlans.index') }}" aria-expanded="false"><i class="mdi mdi-file-document"></i><span class="hide-menu">Video Management</span></a></li>
                
                <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ route('categories.index') }}" aria-expanded="false"><i class="mdi mdi-receipt"></i><span class="hide-menu">Category Management</span></a></li>

                <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ route('subcategories.index') }}" aria-expanded="false"><i class="fa fa-list"></i><span class="hide-menu">Subcategory Management</span></a></li>
                
                <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ route('ageGroups.index') }}" aria-expanded="false"><i class="fa fa-object-group"></i><span class="hide-menu">Age Group Management</span></a></li>
                
                <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ route('routines.index') }}" aria-expanded="false"><i class="fa fa-bicycle"></i><span class="hide-menu">Routine Management</span></a></li>
                
                <!--<li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ route('subscriptions.index') }}" aria-expanded="false"><i class="far fa-list-alt"></i><span class="hide-menu">Subscription Plans</span></a></li>-->

                <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ route('cms.index') }}" aria-expanded="false"><i class="mdi mdi-file"></i><span class="hide-menu">Static Pages</span></a></li>

                <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ route('forums.index') }}" aria-expanded="false"><i class="fa fa-users" aria-hidden="true"></i><span class="hide-menu">Community Forums Details</span></a></li>

                <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ route('reported-forums.index') }}" aria-expanded="false"><i class="fa fa-flag-checkered" aria-hidden="true"></i><span class="hide-menu">Reported Forums</span></a></li>

                <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ route('contactUs.index') }}" aria-expanded="false"><i class="mdi mdi-email-alert"></i><span class="hide-menu">Contact Us Details</span></a></li>
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>