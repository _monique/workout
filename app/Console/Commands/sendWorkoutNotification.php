<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\WorkoutTiming;
use Carbon\Carbon; 
use App\Helper;
use App\Notification;
use Auth;
use Log;
use DB;

class sendWorkoutNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'workout:notification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Notification according to workout planner';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {  
        Log::info('here');
        $today = Carbon::now();Log::info($today);
        $todayDate = $today->timezone('UTC')->format('Y-m-d');Log::info($todayDate);
        $currentTime = $today->timezone('UTC')->format('H:i:s');Log::info($currentTime);
        $addedTime = $today->timezone('UTC')->addMinutes(10)->format('H:i:s');Log::info($addedTime);
    
        DB::enableQueryLog(); 
        $workouts = WorkoutTiming::where('date',$todayDate)->whereBetween('time', [$currentTime , $addedTime])->get();
        Log::info(DB::getQueryLog());
        
        foreach($workouts as $workout) {
        /*  $todayDate = $today->timezone($workout->time_zone)->isoFormat('MMMM Do Y');
            $differenceTime = $today->timezone($workout->time_zone)->subMinutes(10)->isoFormat('h:mm A');*/
            
            Helper::sendNotification($workout->fcmId->fcm_reg_id, 'Your workout is about to start at '.Carbon::parse($workout->time)->timezone($workout->user->time_zone)->format('h:i A'), $workout->fcmId->device, 'workoutPlanDetails', 'Workout Schedule', $workout->workout_plan_id);
            Notification::create(['user_id'=>$workout->user_id, 'title'=>'Workout Schedule' , 'message' => 'Your workout is about to start at '.Carbon::parse($workout->time)->timezone($workout->user->time_zone)->format('h:i A') , 'click_action' => 'workoutPlanDetails', 'action_id' => $workout->workout_plan_id]);
        }
    }
}
