<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommunityForumLike extends Model
{
    protected $fillable = [
        'user_id','like','forum_id',
    ];
}
