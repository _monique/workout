<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Log;
use App\Notification;

class Helper extends Model
{
    
    public static function fcmNotification($fields)
    {
        //Google cloud messaging GCM-API url
        $url = 'https://fcm.googleapis.com/fcm/send';
        $headers = [
            'Authorization: key='.env('FIREBASE_API_KEY'),
            'Content-Type: application/json'
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        Log::info('FCM RESPONSE');
        Log::info($result);
        if ($result === FALSE) {
            die('FCM Send Error:'. curl_error($ch));
        }
        curl_close($ch);
    }
    /**
     * send otp through fcm
     *
     * @param  $fcmToken, $message, $device
     * @return \Illuminate\Http\Response
     */
    public static function sendNotification($fcmToken, $message, $device, $clickAction = null, $image = null, $actionId=null)
    {
        $notification = [
            'body' => $message,
            //'image' => $image!=null?asset('/images/'.$image):asset('/images/'.'dummyUser.jpg'),
            'type' => isset($clickAction) ? $clickAction : null,
            'action_id' => isset($actionId) ? $actionId : null,
            'vibrate'   => 1,
            'sound'     => 1,
        ];
        
        $extraNotificationData = ["message" => $notification, "moredata" =>'Workout'];
        
        if(is_array($fcmToken)){
            $fcmNotification = [
                'registration_ids' => $fcmToken, //multple token array
                'notification' => $notification,
                'data' => $extraNotificationData
            ];
        }else{
            $fcmNotification = [
                'to'        => $fcmToken, //single token
                'notification' => $notification,
                'data' => $extraNotificationData
            ];
        }
        Log::info($notification);
        $result = static::fcmNotification($fcmNotification);

        return $result;
    }
    
    public static function sendBulkNotification($userFcmIds, $title, $message, $clickAction, $actionId = null, $image=null){
        
        foreach($userFcmIds as $userFcmId){
            if($userFcmId['device'] == 'android')
                $androidTokens[] = $userFcmId['fcm_reg_id'];
            else
                $iosTokens[] = $userFcmId['fcm_reg_id'];
                
            Notification::create(['user_id'=>$userFcmId['user_id'], 'title'=>$title , 'message' => $message, 'click_action' => $clickAction, 'action_id' => $actionId]);
        }
        
        if(!empty($androidTokens))
            $result = static::sendNotification($androidTokens, $message, 'android', $clickAction, $image, $actionId);
        if(!empty($iosTokens))
            $result = static::sendNotification($iosTokens, $message, 'ios', $clickAction, $image, $actionId);
        
        return $result;
        
    }
}
