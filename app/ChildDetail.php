<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChildDetail extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'child_name', 'child_height','child_height_unit','child_weight','child_weight_unit','user_id', 'age_group_id'
    ];
    
    public function age(){
        return $this->belongsTo('App\AgeGroup', 'age_group_id', 'id');
    }
}
