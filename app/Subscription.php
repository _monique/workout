<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    protected $fillable = [
        'production_id', 'plan_title','plan_price','plan_period','plan_details', 'plan_currency', 'plan_for',
    ];
}
