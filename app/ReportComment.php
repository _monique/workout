<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportComment extends Model
{
    protected $fillable = [
        'forum_id','user_id','comment_id', 'reason', 'is_reported',
    ];
    
    public function user(){
        return $this->belongsTo('App\User','user_id','id');
    }
    
    public function forum(){
        return $this->belongsTo('App\CommunityForum','forum_id','id');
    }
}
