<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ForumComment extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'user_id','forum_id','comment','created_at','updated_at'
    ];
    
    public function user(){
        return $this->belongsTo('App\User','user_id','id');
    }
}
