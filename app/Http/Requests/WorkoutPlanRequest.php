<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class WorkoutPlanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        switch($this->method())
        { 
            case 'POST': 
            {
                return [
                   'media' => 'required',
                   'name' => 'required|string|max:40',
                   'short_description' => 'required|max:100',
                   'category_id' => 'required',
                   'subcategory_id' => 'required',
                   'age_group_id' => 'required',
                   'routine_id' => 'required',
                   'duration' => 'required|numeric',
                   'calories_burn' => 'required',
                   'calories' => 'required|numeric',
                ];
            }
            case 'PATCH':
            case 'PUT':
            {
                $id = $request->segment(3);
                return [
                   //'media' => 'max:200000',
                   'name' => 'required|string|max:40',
                   'short_description' => 'required|max:100',
                   'category_id' => 'required',
                   'subcategory_id' => 'required',
                   'age_group_id' => 'required',
                   'routine_id' => 'required',
                   'duration' => 'required|numeric',
                   'calories_burn' => 'required',
                   'calories' => 'required|numeric',
                ];
            }
        }
    }

    public function messages()
    {
        return [
            'media.mimes' => 'The media must be an image or video.',
            'short_description.required'=> 'Description is required.',
            //'media.max' => 'The media size should be less than 20 MB.',
           ];
    }
}
