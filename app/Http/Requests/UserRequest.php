<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Auth;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        switch($this->method())
        { 
            case 'POST': 
            {
                return [
                   'email' => 'required|email|unique:users',
                   'name' => 'required|string|max:30',
                   'password' => 'required|min:8|max:16',
                   'password_confirmation' => 'required|same:password|min:8|max:16',
                   'phone_number' => 'required|integer|digits_between:8,15',
                ];
            }
            case 'PATCH':
            case 'PUT':
            {
                $id = $request->segment(3);
                return [
                   'email' => 'required|email|unique:users,email,'.$id,
                   'name' => 'required|string|max:30',
                   'phone_number' => 'required|integer|digits_between:8,15',
                ];
            }
        }
    }
}
