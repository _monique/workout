<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class SubscriptionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        switch($this->method())
        { 
            case 'POST': 
            {
                return [
                   'plan_title' => 'required|max:50',
                   'plan_price' => 'required|numeric',
                   'plan_time' => 'required',
                   'plan_period' => 'required',
                   'plan_details' => 'required|string|max:100',
                ];
            }
            case 'PATCH':
            case 'PUT':
            {
                return [
                   'plan_title' => 'required|max:50',
                   'plan_price' => 'required|numeric',
                   'plan_time' => 'required',
                   'plan_period' => 'required',
                   'plan_details' => 'required|string|max:100',
                ];
            }
        }
    }
}
