<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Auth;
use App\User;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\UserRequest;
use Image;

class ProfileController extends Controller
{
    public function changePassword(){
    	return view('profile.change-password');
    }

    public function updatePassword(Request $request){

    	$input = $request->input();
    	$validator = Validator::make($input,
            [
        		'current_password' => 'required|min:6',
        		'password' => 'required|min:6',
        		'password_confirmation' => 'required|min:6|same:password',
        	]
        );

    	if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
       	}

        if (Hash::check($input['current_password'], Auth::user()->password)) {
            $user = User::findOrFail(Auth::id());
            $user->password = Hash::make($input['password']);
            $user->save();
            flash()->success("Password updated successfully");
        } else {
            flash()->error("Current Password is not correct");
        }
        return redirect()->back();
    }
    
    public function editProfile(){
        
        $user = User::findOrFail(Auth::id());
        
        return view('profile.change-profile', compact('user'));
    }
    
    public function updateProfile(Request $request){

        $input = $request->input(); 
        $id = Auth::id();

    	$validator =  Validator::make($input,[
            'email' => 'required|email|unique:users,email,'.$id,
            'name' => 'required|string|max:50',
            'phone_number' => 'required|numeric',
            'profile_image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
    	]);

        if ($request->has('profile_image')) {
            $originalImage= $request->file('profile_image');
            $thumbnailImage = Image::make($originalImage);
            $thumbnailPath = public_path().'/thumbnail/';
            $originalPath = public_path().'/images/';
            $thumbnailImage->save($originalPath.time().$originalImage->getClientOriginalName());
            $thumbnailImage->resize(150,150);
            $thumbnailImage->save($thumbnailPath.time().$originalImage->getClientOriginalName()); 

            $input['profile_image'] = time().$originalImage->getClientOriginalName();
        }

        $user = User::findOrFail($id);
        $user->fill($input);
        $user->save();

        flash()->success('Profile updated successfully');
        return redirect()->route('dashboard');
    }
}
