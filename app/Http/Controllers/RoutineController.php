<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Routine;
use Validator;
use Image;

class RoutineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $routines = Routine::orderBy('updated_at', 'desc')->get();
        return view('routines.index', compact('routines'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('routines.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->input(); 
        $validator =  Validator::make($input,[
            'name' => 'required|string|max:35',
            //'image' => 'required',
            'description' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        
        if ($request->has('image')) {
            $originalImage= $request->file('image');
            $thumbnailImage = Image::make($originalImage);
            $originalPath = public_path().'/categories/';
            $thumbnailImage->save($originalPath.time().$originalImage->getClientOriginalName());

            $input['image'] = time().$originalImage->getClientOriginalName();
        }

        $routine = new Routine();
        $routine->name = $input['name'];
        $routine->image = $input['image'];
        $routine->description = $input['description'];
        $routine->save();

        flash()->success('New Routine added successfully');
        return redirect()->route('routines.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $routine = Routine::findOrFail($id);
        return view('routines.view', compact('routine'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $routine = Routine::findOrFail($id);
        return view('routines.edit', compact('routine'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->input();
        $validator =  Validator::make($input,[
            'name' => 'required|string|max:35',
            'description' => 'required|string|max:255',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        
        if ($request->has('image')) {
            $originalImage= $request->file('image');
            $thumbnailImage = Image::make($originalImage);
            $originalPath = public_path().'/categories/';
            $thumbnailImage->save($originalPath.time().$originalImage->getClientOriginalName());

            $input['image'] = time().$originalImage->getClientOriginalName();
        }

        $routine = Routine::findOrFail($id);
        $routine->name = $input['name'];
        if ($request->has('image')) 
            $routine->image = $input['image'];
        $routine->description = $input['description'];
        $routine->save();

        flash()->success("Routine updated successfully.");
        return redirect()->route('routines.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Routine::findOrFail($id);
        if ($category->delete()) {
            $response = array(
                'status' => 'success',
                'message' => 'Routine deleted successfully',
            );
        } else {
            $response = array(
                'status' => 'error',
                'message' => 'Routine can not be deleted.Please try again',
            );
        }
        return json_encode($response);
    }

}
