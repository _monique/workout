<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\WorkoutPlanRequest;
use App\WorkoutPlan;
use App\WorkoutTiming;
use App\Category;
use App\AgeGroup;
use App\Routine;
use Image;
use VideoThumbnail;
use App\FcmUser;
use App\Helper;
use App\Subcategory;

class WorkoutPlanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $workoutPlans = WorkoutPlan::get();

        return view('workoutPlans.index', compact('workoutPlans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::orderBy('name', 'ASC')->pluck('name', 'id');
        $ageGroups = AgeGroup::orderBy('name', 'ASC')->pluck('name', 'id');
        $routines = Routine::orderBy('name', 'ASC')->pluck('name', 'id');
        
        return view('workoutPlans.create', compact('categories','ageGroups','routines'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(WorkoutPlanRequest $request)
    {  
        $input = $request->input();

        if ($request->has('media')) {
            
            $file = $request->file('media');
           
            //Move Uploaded File
            $destinationPath = public_path().'/workoutMedia/';
            $fileName = time().'.'.$file->getClientOriginalExtension();
            $file->move($destinationPath,$fileName);
            
            $thumbFileName = time().'thumb.png';
            $storageUrl = public_path().'/workoutMedia/thumbnails';
            VideoThumbnail::createThumbnail($destinationPath.$fileName, $storageUrl, $thumbFileName, 2, $width = 640, $height = 480);

            $input['media'] = $fileName;
            $input['thumbnail'] = $thumbFileName;
        }
    
        $input['calories_burn'] = $input['calories'] .' '. $input['calories_burn']; 
        $input['subcategory_id'] = implode(',', $input['subcategory_id']); 
        $input['age_group_id'] = implode(',', $input['age_group_id']); 
        $input['routine_id'] = implode(',', $input['routine_id']); 
        $workout = WorkoutPlan::create($input);
        
        $userFcmIds = FcmUser::select('fcm_reg_id','device','user_id')->get()->toArray();
        Helper::sendBulkNotification($userFcmIds, 'Workout Plan', 'Admin has added a new Workout Plan.','workoutPlanDetails', $workout->id, null );
        
        flash()->success('New Workout Plan added successfully');
        return redirect()->route('workoutPlans.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $workoutPlan = WorkoutPlan::findOrFail($id);
        $routines = Routine::whereRaw('FIND_IN_SET(?,REPLACE(id,",",","))')->setBindings([$workoutPlan->routine_id])->pluck('name');
        $ageGroups = AgeGroup::whereRaw('FIND_IN_SET(?,REPLACE(id,",",","))')->setBindings([$workoutPlan->age_group_id])->pluck('name');
        $subcategories = Subcategory::whereRaw('FIND_IN_SET(?,REPLACE(id,",",","))')->setBindings([$workoutPlan->subcategory_id])->pluck('name');
        
        return view('workoutPlans.view', compact('workoutPlan','routines','ageGroups','subcategories'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $workoutPlan = WorkoutPlan::findOrFail($id);
        $categories = Category::orderBy('name', 'ASC')->pluck('name', 'id');
        $routines = Routine::orderBy('name', 'ASC')->pluck('name', 'id');
        $ageGroups = AgeGroup::orderBy('name', 'ASC')->pluck('name', 'id');
        $subcategories = Subcategory::where('category_id',$workoutPlan->category_id)->orderBy('name', 'ASC')->pluck('name', 'id');
        $calories = explode(' ' , $workoutPlan->calories_burn);

        return view('workoutPlans.edit', compact('workoutPlan','categories','calories','routines','ageGroups','subcategories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(WorkoutPlanRequest $request, $id)
    {
        $input = $request->input(); 
        $workoutPlan = WorkoutPlan::findOrFail($id);
       
        if ($request->has('media')) {
            
            $file = $request->file('media');
  
            //Move Uploaded File
            $destinationPath = public_path().'/workoutMedia/';
            $fileName = time().'.'.$file->getClientOriginalExtension();
            $file->move($destinationPath,$fileName);
            
            $thumbFileName = time().'thumb.png';
            $storageUrl = public_path().'/workoutMedia/thumbnails';
            VideoThumbnail::createThumbnail($destinationPath.$fileName, $storageUrl, $thumbFileName, 2, $width = 640, $height = 480);

            if(public_path().'/workoutMedia/'.$workoutPlan->media){unlink(public_path().'/workoutMedia/'.$workoutPlan->media);}
            if(public_path().'/workoutMedia/thumbnails/'.$workoutPlan->thumbnail){unlink(public_path().'/workoutMedia/thumbnails/'.$workoutPlan->thumbnail);}
            
            $input['media'] = $fileName;
            $input['thumbnail'] = $thumbFileName;
        }
        
        $input['calories_burn'] = $input['calories'] .' '. $input['calories_burn'];
        $input['subcategory_id'] = implode(',', $input['subcategory_id']); 
        $input['age_group_id'] = implode(',', $input['age_group_id']); 
        $input['routine_id'] = implode(',', $input['routine_id']); 
        $workoutPlan->fill($input);
        $workoutPlan->save();

        flash()->success('Workout Plan updated successfully');
        return redirect()->route('workoutPlans.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $workoutPlan = WorkoutPlan::findOrFail($id);
        WorkoutTiming::where('workout_plan_id', $id)->delete();
        if(public_path().'/workoutMedia/'.$workoutPlan->media){unlink(public_path().'/workoutMedia/'.$workoutPlan->media);}
        if(public_path().'/workoutMedia/thumbnails/'.$workoutPlan->thumbnail){unlink(public_path().'/workoutMedia/thumbnails/'.$workoutPlan->thumbnail);}
            
        if ($workoutPlan->delete()) {
            $response = array(
                'status' => 'success',
                'message' => 'Workout Plan deleted successfully',
            );
        }
        
        return json_encode($response);
    }
    
    public function deactivate($id)
    {
        $workoutPlan = WorkoutPlan::find($id);
        $workoutPlan->is_activated = !$workoutPlan->is_activated;
        $workoutPlan->save();

        if ($workoutPlan->is_activated)
            flash()->success("Workout Plan activated successfully.");
        else
            flash()->success("Workout Plan de-activated successfully.");

        return redirect()->route('workoutPlans.index');
    }
    
    public function getSubcategory(Request $request){
        $id = $request->input('category_id');
        $subcategories = Subcategory::where('category_id', $id)->get();
        
        return $subcategories;
        
    }
}
