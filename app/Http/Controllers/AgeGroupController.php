<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AgeGroup;
use Validator;
use Image;

class AgeGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ageGroups = AgeGroup::orderBy('updated_at', 'desc')->get();
        return view('ageGroups.index', compact('ageGroups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('ageGroups.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->input(); 
        $validator =  Validator::make($input,[
            'name' => 'required|string|max:35',
            //'image' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        
        if ($request->has('image')) {
            $originalImage= $request->file('image');
            $thumbnailImage = Image::make($originalImage);
            $originalPath = public_path().'/categories/';
            $thumbnailImage->save($originalPath.time().$originalImage->getClientOriginalName());

            $input['image'] = time().$originalImage->getClientOriginalName();
        }

        $ageGroup = new AgeGroup();
        $ageGroup->name = $input['name'];
        if ($request->has('image'))
        $ageGroup->image = $input['image'];
        $ageGroup->save();

        flash()->success('New Age Group added successfully');
        return redirect()->route('ageGroups.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ageGroup = AgeGroup::findOrFail($id);
        return view('ageGroups.view', compact('ageGroup'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ageGroup = AgeGroup::findOrFail($id);
        return view('ageGroups.edit', compact('ageGroup'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->input();
        $validator =  Validator::make($input,[
            'name' => 'required|string|max:35',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        
        if ($request->has('image')) {
            $originalImage= $request->file('image');
            $thumbnailImage = Image::make($originalImage);
            $originalPath = public_path().'/categories/';
            $thumbnailImage->save($originalPath.time().$originalImage->getClientOriginalName());

            $input['image'] = time().$originalImage->getClientOriginalName();
        }

        $ageGroup = AgeGroup::findOrFail($id);
        $ageGroup->name = $input['name'];
        if ($request->has('image'))
        $ageGroup->image = $input['image'];
        $ageGroup->save();

        flash()->success("Age Group updated successfully.");
        return redirect()->route('ageGroups.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ageGroup = AgeGroup::findOrFail($id);
        if ($ageGroup->delete()) {
            $response = array(
                'status' => 'success',
                'message' => 'Age Group deleted successfully.',
            );
        } else {
            $response = array(
                'status' => 'error',
                'message' => 'Age Group can not be deleted.Please try again.',
            );
        }
        return json_encode($response);
    }

}
