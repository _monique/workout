<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Requests\UserRequest;
use Image;
use App\CommunityForum;
use App\CommunityForumLike;
use App\ForumComment;
use App\Inquiry;
use App\ChildDetail;
use App\ReportPost;
use App\WorkoutTiming;
use App\Notification;
use App\FcmUser;
use Excel;
use App\Exports\UsersExport;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('role_id', '!=' ,'1')->orderBy('id','DESC')->get();
        return view('users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $input = $request->input();
        $input['role_id'] = 2;
        $input['password'] = bcrypt($input['password']);

        if ($request->has('profile_image')) {
            $originalImage= $request->file('profile_image');
            $thumbnailImage = Image::make($originalImage);
            $originalPath = public_path().'/images/';
            $thumbnailImage->save($originalPath.time().$originalImage->getClientOriginalName());

            $input['profile_image'] = time().$originalImage->getClientOriginalName();
        }

        $user = User::create($input);
        $user->sendApiEmailVerificationNotification();

        flash()->success('New User added successfully');
        return redirect()->route('users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {       
        $user = User::findOrFail($id);
        $user->childDetails;
        return view('users.view', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        return view('users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
        $input = $request->input(); 

        if ($request->has('profile_image')) {
            $originalImage= $request->file('profile_image');
            $thumbnailImage = Image::make($originalImage);
            $originalPath = public_path().'/images/';
            $thumbnailImage->save($originalPath.time().$originalImage->getClientOriginalName());

            $input['profile_image'] = time().$originalImage->getClientOriginalName();
        }

        $user = User::findOrFail($id);
        $user->fill($input);
        $user->save();

        flash()->success('User updated successfully');
        return redirect()->route('users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    { 
        $user = User::findOrFail($id);
        $user->OauthAcessToken()->delete();
        CommunityForum::where('user_id',$id)->delete();
        CommunityForumLike::where('user_id',$id)->delete();
        ForumComment::where('user_id',$id)->delete();
        Inquiry::where('user_id', $id)->delete();
        ChildDetail::where('user_id', $id)->delete();
        ReportPost::where('user_id', $id)->delete();
        WorkoutTiming::where('user_id', $id)->delete();
        Notification::where('user_id', $id)->delete();
        FcmUser::where('user_id', $id)->delete();
        
        if ($user->delete()) {
            $response = array(
                'status' => 'success',
                'message' => 'User deleted successfully',
            );
        }

        return json_encode($response);
    }
    
    public function block($id)
    {
        $user = User::find($id);
        $user->is_blocked = !$user->is_blocked;
        $user->save();

        if ($user->is_blocked)
            flash()->success("User blocked successfully.");
        else
            flash()->success("User Unblocked successfully.");

        return redirect()->route('users.index');
    }
    
    /**
    * @return \Illuminate\Support\Collection
    */
    public function export() 
    {
        return Excel::download(new UsersExport, 'users.xlsx');
    }
}
