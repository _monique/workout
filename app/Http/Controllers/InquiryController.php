<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Inquiry;

class InquiryController extends Controller
{
    public function index()
    {
    	$inquiries = Inquiry::orderBy('id','DESC')->get();

        return view('inquiries.index', compact('inquiries'));
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    { 
        $user = Inquiry::findOrFail($id);
        
        if ($user->delete()) {
            $response = array(
                'status' => 'success',
                'message' => 'Contact Us Data deleted successfully',
            );
        }

        return json_encode($response);
    }
}
