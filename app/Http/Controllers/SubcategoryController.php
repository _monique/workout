<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subcategory;
use App\Category;
use Validator;
use Image;

class SubcategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subcategories = Subcategory::orderBy('updated_at', 'desc')->get();
        return view('subcategories.index', compact('subcategories'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::orderBy('name', 'ASC')->get()->toArray();

        return view('subcategories.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->input(); 
        $validator =  Validator::make($input,[
            'name' => 'required|string|max:35',
            'category_id' => 'required',
            'description' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        
        if ($request->has('image')) {
            $originalImage= $request->file('image');
            $thumbnailImage = Image::make($originalImage);
            $originalPath = public_path().'/categories/';
            $thumbnailImage->save($originalPath.time().$originalImage->getClientOriginalName());

            $input['image'] = time().$originalImage->getClientOriginalName();
        }
        
        $category = new Subcategory();
        $category->name = $input['name'];
        $category->image = $input['image'];
        $category->category_id = $input['category_id'];
        $category->description = $input['description'];
        $category->save();

        flash()->success('New Sub Category added successfully');
        return redirect()->route('subcategories.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subcategory = Subcategory::findOrFail($id);
        $categories = Category::orderBy('name', 'ASC')->get()->toArray();
        
        return view('subcategories.edit', compact('subcategory','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->input();
        $validator =  Validator::make($input,[
            'name' => 'required|string|max:35',
            'category_id' => 'required',
            'description' => 'required|max:255'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        
        if ($request->has('image')) {
            $originalImage= $request->file('image');
            $thumbnailImage = Image::make($originalImage);
            $originalPath = public_path().'/categories/';
            $thumbnailImage->save($originalPath.time().$originalImage->getClientOriginalName());

            $input['image'] = time().$originalImage->getClientOriginalName();
        }
        
        $category = Subcategory::findOrFail($id);
        $category->name = $input['name'];
        if ($request->has('image')) 
            $category->image = $input['image'];
        $category->category_id = $input['category_id'];
        $category->description = $input['description'];
        $category->save();

        flash()->success("Sub Category updated successfully.");
        return redirect()->route('subcategories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Subcategory::findOrFail($id);
        if ($category->delete()) {
            $response = array(
                'status' => 'success',
                'message' => 'Sub Category deleted successfully',
            );
        } else {
            $response = array(
                'status' => 'error',
                'message' => 'Sub Category can not be deleted.Please try again',
            );
        }
        return json_encode($response);
    }
}
