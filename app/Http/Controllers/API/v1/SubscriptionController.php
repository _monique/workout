<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Subscription;
use App\UserSubscription;
use Auth;

class SubscriptionController extends Controller
{
    public $successStatus = 200;
    public $errorStatus = 400;
    
    public function subscriptionPlans(Request $request)
    {

        $response = [];
        
        foreach($request->input() as $key => $inputValue) {
            $response[$key] = gettype($inputValue);
        }
        \Log::info($response);
        \Log::info($request);
        
        $input = $request->input();
        $purchaseSubscription = json_decode($input['data'], true);
        $data = json_decode($input['subscription'], true);

        \Log::info($data);
        \Log::info($purchaseSubscription);
        
        
        if ($input['device_type'] == 'android') {
        
            foreach($data as $val)
            {
                $sub = new Subscription();
                $sub->production_id = $val['productId']; 
                $sub->plan_title = $val['title'];
                $sub->plan_currency = $val['currency']; 
                $sub->plan_price = $val['price']; 
                $sub->plan_period = $val['subscriptionPeriodAndroid']; 
                $sub->plan_details = $val['description'];
                $sub->plan_for = 'android';
                $sub->save();
            }
            
            $plan = Subscription::where('production_id', $purchaseSubscription['productId'])->first();
            
            $subscription = new UserSubscription();
            $subscription->user_id = Auth::id();
            $subscription->name = $plan->plan_title;
            $subscription->production_id = $purchaseSubscription['productId']; 
            $subscription->transaction_id = $purchaseSubscription['transactionId'];
            $subscription->validity = $plan->plan_period;
            $subscription->price = $plan->plan_currency.''.$plan->plan_price;
            $subscription->purchase_date = date('Y-m-d h:i:s', $purchaseSubscription['transactionDate'] / 1000);
            $subscription->device_type = $input['device_type'];
            $subscription->is_autorenewal = $purchaseSubscription['autoRenewingAndroid'];
            
            if ($subscription->save())
                return response()->json(['status_code' => $this->successStatus , 'message' => 'Subscription save successfully.', 'data' => null]);
        } else {
            foreach($data as $val)
            {
                $sub = Subscription::where('production_id', $val['productId'])->get();
                
                if(count($sub) == 0) {
                    $sub = new Subscription();
                    $sub->production_id = $val['productId']; 
                    $sub->plan_title = $val['title'];
                    $sub->plan_currency = $val['currency']; 
                    $sub->plan_price = $val['price']; 
                    $sub->plan_period = $val['subscriptionPeriodUnitIOS']; 
                    $sub->plan_details = $val['description'];
                    $sub->plan_for = 'ios';
                    $sub->save();
                }
            }
            
            $plan = Subscription::where('production_id', $purchaseSubscription['product_id'])->first();
            
            $subscription = new UserSubscription();
            $subscription->user_id = Auth::id();
            $subscription->name = $plan->plan_title;
            $subscription->production_id = $purchaseSubscription['product_id']; 
            $subscription->transaction_id = $purchaseSubscription['transaction_id'];
            $subscription->validity = $plan->plan_period;
            $subscription->price = $plan->plan_currency.''.$plan->plan_price;
            $subscription->purchase_date = date('Y-m-d h:i:s', $purchaseSubscription['purchase_date_ms'] / 1000);
            $subscription->end_date = date('Y-m-d h:i:s', $purchaseSubscription['expires_date_ms'] / 1000);
            $subscription->device_type = 'ios';
            //$subscription->is_autorenewal = $purchaseSubscription['autoRenewingAndroid'];
            
            if ($subscription->save())
                return response()->json(['status_code' => $this->successStatus , 'message' => 'Subscription save successfully.', 'data' => null]);
        }
    }
    
}
