<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\CommunityForum;
use Validator;
use Auth;
use Image;
use App\CommunityForumLike;
use App\ForumComment;
use App\ReportPost;
use App\ReportComment;
use Carbon\Carbon;
use Str;
use App\FcmUser;
use App\Helper;
use App\Notification;
use Log;

class CommunityForumController extends Controller
{
    public $successStatus = 200;
    public $errorStatus = 400;

    /**
     * Create new forum api
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function new_forum(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|string|max:200',
            'media' => 'required',
            'description' => 'required|string|max:1000',
            'created_at' => 'required'
        ]);        

        if ($validator->fails())
            return response()->json(['status_code'=> $this->errorStatus, 'message'=> $validator->errors()->first()]);

        $input = $request->all();       
        
        $data = $request->input('media');
        
        $image_parts = explode(";base64,", $data);
        $image_type_aux = explode("image/", $image_parts[0]);
        $image_type = $image_type_aux[1];
        $image_base64 = base64_decode($image_parts[1]);

        $fileName = md5(uniqid(rand(), true));

        $image = $fileName.'.'.$image_type;

        $file = public_path().'/communityForums/'.$image;

        $success = file_put_contents($file, $image_base64);

        $img = Image::make(public_path().'/communityForums/'.$image);

        $input['media'] = $image;

        $input['user_id'] = Auth::id();        
        $input['updated_at'] = $input['created_at'];        
        $forum = CommunityForum::create($input);
        
        $userFcmIds = FcmUser::select('fcm_reg_id','device','user_id')->where('user_id','!=',Auth::id())->get()->toArray();
        \Log::info($userFcmIds);
        
        Helper::sendBulkNotification($userFcmIds, 'Community Forum', Auth::user()->name.' has added a new Community Forum.', 'forumListing', $forum->id, Auth::user()->profile_image);
        
        return response()->json(['status_code' => $this->successStatus, 'message' => 'Community Forum created successfully.']);
    }
    
    
    /**
     * Edit forum api
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function edit_forum(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'title' => 'required|string|max:200',
            'description' => 'required|string|max:1000', 
            'media' => 'required', 
            ]);        

        if ($validator->fails())
            return response()->json(['status_code'=> $this->errorStatus, 'message'=> $validator->errors()->first()]);

        $input = $request->all(); 
                
        $forum = CommunityForum::findOrFail($input['id']);
        
        if($request->input('media')){
            
            $data = $request->input('media');
            
            if(Str::startsWith( $data, 'data' )){

                $image_parts = explode(";base64,", $data);
                $image_type_aux = explode("image/", $image_parts[0]);
                $image_type = $image_type_aux[1];
                $image_base64 = base64_decode($image_parts[1]);
        
                $fileName = md5(uniqid(rand(), true));
        
                $image = $fileName.'.'.$image_type;
        
                $file = public_path().'/communityForums/'.$image;
        
                $success = file_put_contents($file, $image_base64);
        
                $img = Image::make(public_path().'/communityForums/'.$image);
        
                $communityForum['media'] = $image;
                
                unlink(public_path().'/communityForums/'.$forum->media);
                $communityForum['title'] = $input['title'];
                $communityForum['description'] = $input['description'];
            }else{
                $communityForum['title'] = $input['title'];
                $communityForum['description'] = $input['description'];
            }
        }
        $forum->fill($communityForum);
    
        if($forum->save())
            return response()->json(['status_code' => $this->successStatus, 'message' => 'Forum updated successfully.']);
        else
            return response()->json(['status_code' => $this->errorStatus, 'message' => 'Forum can not be updated successfully.']);
    }
    
    /**
     * Delete Community Forum API 
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function delete_forum(Request $request)
    {
        $input = $request->input();
        $forum = CommunityForum::findOrFail($input['id']);
        ReportComment::where('forum_id',$input['id'])->delete();
        ForumComment::where('forum_id',$input['id'])->delete();
        CommunityForumLike::where('forum_id',$input['id'])->delete();
        ReportPost::where('forum_id',$input['id'])->delete();
        
        if($forum->delete()){
            if($forum->media != null)
                unlink(public_path().'/communityForums/'.$forum->media);
                
            $communityForums = CommunityForum::where('is_reported',0)->where('user_id', Auth::id())->withCount('totalLikes')->withCount('comments')->withCount('is_liked')->orderBy('id', 'DESC')->get();
            foreach($communityForums as $key => $forum)
            {
                $forum->media = asset('/communityForums/'.$forum->media);
                $forum->postedBy = $forum->user->name;
                $forum->user_profile_image = $forum->user->profile_image!=null?asset('/images/'.$forum->user->profile_image):asset('/images/'.'dummyUser.jpg');
            }
            return response()->json(['status_code' => $this->successStatus , 'message' => 'Community Forum deleted successfully.', 'data' => $communityForums]);
        }else{
            return response()->json(['status_code' => $this->errorStatus , 'message' => 'Community Forum not deleted successfully.', 'data' => null]);
        }
    }
    
    /**
     * Like Community Forum API 
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function like_unlike_forum(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'like' => 'required|boolean',
        ]);        

        if ($validator->fails())
            return response()->json(['status_code'=> $this->errorStatus, 'message'=> $validator->errors()->first()]);
            
        $input = $request->input();
        
        $is_liked = CommunityForumLike::where('forum_id', $input['id'])->where('user_id', Auth::id())->get();
        
        if(count($is_liked) > 0)
        {
            if($input['like'] == 1)
            {
                CommunityForumLike::where('forum_id', $input['id'])->where('user_id', Auth::id())->update(['like' => $input['like']]);
                $success['count'] = CommunityForumLike::where('forum_id', $input['id'])->count();
                
                $forum = CommunityForum::where('id',$input['id'])->with('fcmId')->with('user')->get();
                
                if($forum[0]->fcmId->user_id != Auth::id()){
                    Helper::sendNotification($forum[0]->fcmId->fcm_reg_id, Auth::user()->name.' has liked your forum', $forum[0]->fcmId->device, 'forumDetails', Auth::user()->profile_image, $forum[0]->id);
                    Notification::create(['user_id'=>$forum[0]->fcmId->user_id, 'title'=>'Forum Liked' , 'message' => Auth::user()->name.' has liked your forum','click_action' => 'forumDetails', 'action_id' => $forum[0]->id]);
                }else{
                    $users = CommunityForumLike::select('user_id')->where('forum_id', $input['id'])->get()->toArray();
                    $userFcmIds = FcmUser::select('fcm_reg_id','device','user_id')->where('user_id','!=',Auth::id())->whereIn('user_id',$users)->get()->toArray();
                    Helper::sendBulkNotification($userFcmIds, 'Forum Liked', Auth::user()->name.' has liked on their own Forum', 'forumDetails', $forum[0]->id, Auth::user()->profile_image);
                }
                
                return response()->json(['status_code' => $this->successStatus , 'message' => 'Community Forum liked successfully.', 'data' => $success]);
            }else if($input['like'] == 0)
            {
                CommunityForumLike::where('forum_id', $input['id'])->where('user_id', Auth::id())->delete();
                $success['count'] = CommunityForumLike::where('forum_id', $input['id'])->count();
                return response()->json(['status_code' => $this->successStatus , 'message' => 'Community Forum unliked successfully.', 'data' => $success]);
            }
        }else
        {
            $communityLike = new CommunityForumLike;
            $communityLike->user_id = Auth::id();
            $communityLike->forum_id = $input['id'];
            $communityLike->like = $input['like'];
            $communityLike->save();
          
            $success['count'] = CommunityForumLike::where('forum_id', $input['id'])->count();
            $forum = CommunityForum::where('id',$input['id'])->with('fcmId')->with('user')->get();
            
            if($forum[0]->fcmId->user_id != Auth::id()){
                Helper::sendNotification($forum[0]->fcmId->fcm_reg_id, Auth::user()->name.' has liked your forum', $forum[0]->fcmId->device, 'forumDetails', Auth::user()->profile_image, $forum[0]->id);
                Notification::create(['user_id'=>$forum[0]->fcmId->user_id, 'title'=>'Forum Liked' , 'message' => Auth::user()->name.' has liked your forum','click_action' => 'forumDetails', 'action_id' => $forum[0]->id]);
            }else{
                $users = CommunityForumLike::select('user_id')->where('forum_id', $input['id'])->get()->toArray();
                $userFcmIds = FcmUser::select('fcm_reg_id','device','user_id')->where('user_id','!=',Auth::id())->whereIn('user_id',$users)->get()->toArray();
                Helper::sendBulkNotification($userFcmIds, 'Forum Liked', Auth::user()->name.' has liked on their own Forum', 'forumDetails', $forum[0]->id, Auth::user()->profile_image);
            }
            
            return response()->json(['status_code' => $this->successStatus , 'message' => 'Community Forum liked successfully.', 'data' => $success]);
        }
        
        
        return response()->json(['status_code' => $this->errorStatus , 'message' => 'Please try again.', 'data' => null]);
    }
    
    public function addComment(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'comment' => 'required|max:200',
            'created_at' => 'required'
        ]);        

        if ($validator->fails())
            return response()->json(['status_code'=> $this->errorStatus, 'message'=> $validator->errors()->first()]);
            
        $input = $request->input();
        $input['user_id'] = Auth::id();
        $input['forum_id'] = $input['id'];
        $input['created_at'] = $input['created_at'];
        $input['updated_at'] = $input['created_at'];
        $forum = ForumComment::create($input);
        
        $forumcomments = ForumComment::where('forum_id',$input['forum_id'])->with('user')->orderBy('id', 'DESC')->get();
        foreach($forumcomments as $comment){
            $comment->user->profile_image = $comment->user->profile_image != ''? asset('/images/'.$comment->user->profile_image):asset('/images/'.'dummyUser.jpg');
        }
        $comments['comments'] = $forumcomments;
        $comments['totalComments'] = count($forumcomments);
        
        $forum = CommunityForum::where('id',$input['id'])->with('fcmId')->with('user')->get();
        if($forum[0]->fcmId->user_id != Auth::id()){
            Helper::sendNotification($forum[0]->fcmId->fcm_reg_id, Auth::user()->name.' commented on your forum', $forum[0]->fcmId->device, 'forumDetails', Auth::user()->profile_image, $forum[0]->id);
            Notification::create(['user_id'=>$forum[0]->fcmId->user_id, 'title'=>'Comment on forum' , 'message' => Auth::user()->name.' commented on your forum','click_action' => 'forumDetails', 'action_id' => $forum[0]->id]);
        }else{
            $users = ForumComment::select('user_id')->where('forum_id', $input['id'])->get()->toArray();
            $userFcmIds = FcmUser::select('fcm_reg_id','device','user_id')->where('user_id','!=',Auth::id())->whereIn('user_id',$users)->get()->toArray();
            if (!empty($userFcmIds))
                Helper::sendBulkNotification($userFcmIds, 'Comment on forum', Auth::user()->name.' has commented on their own Forum', 'forumDetails', $forum[0]->id, Auth::user()->profile_image);
        }
        
        return response()->json(['status_code'=> $this->successStatus, 'message'=> 'Comment added successfully.', 'data' => $comments]);
        
    }

    public function deleteComment(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'comment_id' => 'required',
        ]);        

        if ($validator->fails())
            return response()->json(['status_code'=> $this->errorStatus, 'message'=> $validator->errors()->first()]);

        if (ForumComment::where('id', $request->comment_id)->delete())
            return response()->json(['status_code' => $this->successStatus , 'message' => 'Comment deleted successfully.', 'data' => null]);
        else
            return response()->json(['status_code' => $this->errorStatus , 'message' => 'Comment not deleted successfully.Please try again!', 'data' => null]);
    }
    
    public function forum_listing(Request $request)
    {
        $communityForums = CommunityForum::where('is_reported',0)->withCount('totalLikes')->withCount('comments')->withCount('is_liked')->orderBy('id','DESC')->skip($request->skip)->take($request->take)->get(); //->skip($request->skip)->take($request->take)
        foreach($communityForums as $key => $forum)
        {
            $forum->media = asset('/communityForums/'.$forum->media);
            $forum->postedBy = $forum->user->name;
            $forum->user_profile_image = $forum->user->profile_image!=null?asset('/images/'.$forum->user->profile_image):asset('/images/'.'dummyUser.jpg');
        }
        
        if(count($communityForums) > 0)
            return response()->json(['status_code' => $this->successStatus, 'message' => '', 'data' => $communityForums]);
        else
            return response()->json(['status_code' => $this->errorStatus, 'message' => 'There are no Community Forums available.', 'data' => '']);
    }
    
    public function comment_listing(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'forum_id' => 'required',
        ]);        

        if ($validator->fails())
            return response()->json(['status_code'=> $this->errorStatus, 'message'=> $validator->errors()->first()]);
            
        $input = $request->input();
        $forumcomments = ForumComment::where('forum_id',$input['forum_id'])->with('user')->orderBy('id', 'DESC')->get();
        foreach($forumcomments as $comment){
            $comment->user_profile_image = $comment->user->profile_image!=null? asset('/images/'.$comment->user->profile_image):asset('/images/'.'dummyUser.jpg');
            $comment->my_comment = ($comment->user_id == Auth::id()) ? 1 : 0;
        }
        $comments['comments'] = $forumcomments;
        $comments['totalComments'] = count($forumcomments);
        
        if(count($comments) > 0)
            return response()->json(['status_code' => $this->successStatus, 'message' => '', 'data' => $comments]);
        else
            return response()->json(['status_code' => $this->errorStatus, 'message' => 'There are no Comments available.', 'data' => '']);
    }
    
    public function report_forum(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'forum_id' => 'required',
            'reason' => 'required|string'
        ]); 

        if ($validator->fails())
            return response()->json(['status_code'=> $this->errorStatus, 'message'=> $validator->errors()->first()]);
            
        $input = $request->input();
        $report = ReportPost::where('forum_id',$input['forum_id'])->where('user_id', Auth::id())->get();
        
        if(count($report) > 0)
            return response()->json(['status_code' => $this->errorStatus, 'message' => 'Already Reported', 'data' => '']);
        else{
            
            $input['user_id'] = Auth::id();
            $input['forum_id'] = $input['forum_id'];
            ReportPost::create($input);
            
            $count = ReportPost::where('forum_id',$input['forum_id'])->get()->count();
            if($count > 4)
                CommunityForum::where('id', $input['forum_id'])->update(['is_reported' => 1]);
            
            return response()->json(['status_code' => $this->successStatus, 'message' => 'Forum Reported successfully.', 'data' => '']);
        }
    }

    public function report_comment(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'forum_id' => 'required',
            'comment_id' => 'required',
            'reason' => 'required|string|max:255',
        ]); 

        if ($validator->fails())
            return response()->json(['status_code'=> $this->errorStatus, 'message'=> $validator->errors()->first()]);
            
        $input = $request->input();
        $report = ReportComment::where('forum_id',$input['forum_id'])->where('user_id', Auth::id())->get();
        
        if(count($report) > 0)
            return response()->json(['status_code' => $this->errorStatus, 'message' => 'Already Reported', 'data' => '']);
        else{
            
            $input['user_id'] = Auth::id();
            $input['forum_id'] = $input['forum_id'];
            ReportComment::create($input);
            
            $count = ReportComment::where('comment_id',$input['comment_id'])->get()->count();
            if($count > 4)
                ReportComment::where('comment_id', $input['comment_id'])->update(['is_reported' => 1]);
            
            return response()->json(['status_code' => $this->successStatus, 'message' => 'Comment Reported successfully.', 'data' => '']);
        }
    }
    
    public function my_posts(Request $request)
    {
        $communityForums = CommunityForum::where('is_reported',0)->where('user_id', Auth::id())->withCount('totalLikes')->withCount('comments')->withCount('is_liked')->orderBy('id', 'DESC')->get();
        foreach($communityForums as $key => $forum)
        {
            $forum->media = asset('/communityForums/'.$forum->media);
            $forum->postedBy = $forum->user->name;
            $forum->user_profile_image = $forum->user->profile_image!=null?asset('/images/'.$forum->user->profile_image):asset('/images/'.'dummyUser.jpg');
        }
        
        if(count($communityForums) > 0)
            return response()->json(['status_code' => $this->successStatus, 'message' => '', 'data' => $communityForums]);
        else
            return response()->json(['status_code' => $this->errorStatus, 'message' => 'There are no Community Forums available.', 'data' => '']);
    }
}
