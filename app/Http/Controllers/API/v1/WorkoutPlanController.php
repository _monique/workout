<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\WorkoutPlan;
use App\Category;
use App\Subcategory;
use App\WorkoutTiming;
use App\Routine;
use App\AgeGroup;
use Auth;
use Validator;

class WorkoutPlanController extends Controller
{
    public $successStatus = 200;
    public $errorStatus = 400;
    
    public function workoutPlanList(Request $request)
    {
        if(!empty($request->input('category_id'))&& ($request->input('category_id') != "null")){
            $category_id = $request->input('category_id');
            $workoutPlans = WorkoutPlan::where('category_id',$category_id)->where('is_activated', 1)->with('workoutSchedule')->get();
        }else if(!empty($request->input('search'))&& ($request->input('search') != "null")){
            $search = $request->input('search');
            $workoutPlans = WorkoutPlan::where('name','LIKE','%'.$search.'%')->orWhere('short_description','LIKE','%'.$search.'%')->where('is_activated', 1)->with('workoutSchedule')->get();
        }else{
            $workoutPlans = WorkoutPlan::where('is_activated', 1)->with('workoutSchedule')->get();
        }
        
        /*foreach($workoutPlans as $key => $plan){
            $workoutPlans[$key]->media = asset('/workoutMedia/'.$plan->media);
            $workoutPlans[$key]->thumbnail = asset('/workoutMedia/thumbnails/'.$plan->thumbnail);
            $workoutPlans[$key]->category_id = $workoutPlans[$key]->category_id != null ? $workoutPlans[$key]->category->name : null;
        }*/
        
        if (count($workoutPlans) > 0)
            return response()->json(['status_code' => $this->successStatus , 'message' => '', 'data' => $workoutPlans]);
        else
            return response()->json(['status_code' => $this->errorStatus , 'message' => 'There are no workout plans.', 'data' => null]);
    }
    
    public function categoriesList(Request $request)
    {
        $success['categories'] = Category::select('id','name')->orderBy('name', 'ASC')->get()->toArray();
        $user = Auth::user();
        $success['selectedCategory'] = $user->health_goal;
        
        if (count($success['categories']) > 0)
            return response()->json(['status_code' => $this->successStatus , 'message' => '', 'data' => $success]);
        else
            return response()->json(['status_code' => $this->errorStatus , 'message' => 'There are no categories available.', 'data' => null]);
        
    }
    
    public function addWorkoutTimings(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'workout_plan_id' => 'required',
            'date' => 'required',
            'time' => 'required',
        ]);        

        if ($validator->fails())
            return response()->json(['status_code'=> $this->errorStatus, 'message'=> $validator->errors()->first()]);
            
        $time = Carbon::parse($request->date.''.$request->time, Auth::user()->time_zone)->setTimezone('UTC'); 
        $date = Carbon::parse($request->date)->setTimezone('UTC')->format('Y-m-d'); 
        $currentTime = Carbon::now()->setTimezone('UTC');
         
        $schedule =  WorkoutTiming::where('user_id', Auth::id())->where('date', $date)->where('time', $time->format('H:i:s'))->get(); 
        
        if ($schedule->count() > 0)
            return response()->json(['status_code'=> $this->errorStatus, 'message'=> 'Workout is already scheduled.']);

        $diff = $time->diffInMinutes($currentTime); 
        
        if ($diff < 30)
            return response()->json(['status_code'=> $this->errorStatus, 'message'=> 'Please schedule the workout more than 30 mins from the current time.']);
            
        if ($time->lt($currentTime))
            return response()->json(['status_code'=> $this->errorStatus, 'message'=> 'Sorry!! you cannot schedule the workout in the past time.']);
          
        $input = $request->input();
        $input['user_id'] = Auth::id();
        $input['date'] = $date;
        $input['time'] = $time->format('H:i:s'); //dd($input);
        $workoutTiming = WorkoutTiming::create($input);
        
        return response()->json(['status_code'=> $this->successStatus, 'message'=> 'Thank you!! your workout schedule has been added successfully.', 'data' => '']);
        
    }
    
    public function updateWorkoutTimings(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'date' => 'required',
            'time' => 'required',
        ]);        

        if ($validator->fails())
            return response()->json(['status_code'=> $this->errorStatus, 'message'=> $validator->errors()->first()]);
        
        $input = $request->input();
        $workoutTiming = WorkoutTiming::findOrFail($input['id']);
        
        if($workoutTiming){
            $workoutTiming->fill($input);
            $workoutTiming->save();
            return response()->json(['status_code' => $this->successStatus, 'message' => 'Thank you!! your workout schedule has been updated successfully.','data' => '']);
        }else{
            return response()->json(['status_code' => $this->errorStatus, 'message' => 'Your workout schedule can not updated successfully.','data' => '']);
        }
        
    }
    
    public function dashboard(Request $request)
    {
        $success['routines'] = Routine::get();
        $success['ageGroups'] = AgeGroup::get();
        $success['categories'] = Category::get();
        
        return response()->json(['status_code' => $this->successStatus , 'message' => '', 'data' => $success]);
    }
    
    public function workoutsByCategory(Request $request)
    {
        $input = $request->input();
        $success['ageGroups'] = AgeGroup::get();
        $success['subcategories'] = Subcategory::where('category_id',$input['category_id'])->get();
        
        if (!empty($request->input('subcategory_id'))&& ($request->input('subcategory_id') != "null") && !empty($request->input('age_group_id'))&& ($request->input('age_group_id') != "null")){
            $cat = $request->input('category_id');
            $subcat = $request->input('subcategory_id');
            $age = $request->input('age_group_id');
            
            $success['workoutPlans'] = WorkoutPlan::where('is_activated', 1)
            ->where('category_id',$input['category_id'])
            ->whereRaw('FIND_IN_SET(?,REPLACE(subcategory_id,",",","))')
            ->whereRaw('FIND_IN_SET(?,REPLACE(age_group_id,",",","))')
            ->with('workoutSchedule')
            ->setBindings([1, $cat, $subcat, $age])
            ->get();
        
        } else if (!empty($request->input('age_group_id'))&& ($request->input('age_group_id') != "null")){
            $cat = $request->input('category_id');
            $age = $request->input('age_group_id');
            \DB::enableQueryLog();
            $success['workoutPlans'] = WorkoutPlan::where('is_activated', 1)
            ->where('category_id',$input['category_id'])
            ->whereRaw('FIND_IN_SET(?,REPLACE(age_group_id,",",","))')
            ->with('workoutSchedule')
            ->setBindings([1, $cat, $age])
            ->get();
            //dd(\DB::getQueryLog());
           
        } else if (!empty($request->input('subcategory_id'))&& ($request->input('subcategory_id') != "null")){
            $cat = $request->input('category_id');
            $subcat = $request->input('subcategory_id');
            
            $success['workoutPlans'] = WorkoutPlan::where('is_activated', 1)
            ->where('category_id',$input['category_id'])
            ->whereRaw('FIND_IN_SET(?,REPLACE(subcategory_id,",",","))')
            ->with('workoutSchedule')
            ->setBindings([1, $cat, $subcat])
            ->get();
            
        } else {
            $success['workoutPlans'] = WorkoutPlan::where('is_activated', 1)->where('category_id',$input['category_id'])->with('workoutSchedule')->get();
         }  
        return response()->json(['status_code' => $this->successStatus , 'message' => '', 'data' => $success]);
    }
    
    public function workoutsByRoutine(Request $request)
    {
        $input = $request->input();
        $routine = $request->input('routine_id');
        $success['ageGroups'] = AgeGroup::get();
        
        if (!empty($request->input('age_group_id'))&& ($request->input('age_group_id') != "null")){
            $age = $request->input('age_group_id');
            
            $success['workoutPlans'] = WorkoutPlan::where('is_activated', 1)
            ->whereRaw('FIND_IN_SET(?,REPLACE(routine_id,",",","))')
            ->whereRaw('FIND_IN_SET(?,REPLACE(age_group_id,",",","))')
            ->with('workoutSchedule')
            ->setBindings([1,$routine, $age])
            ->get();
        } else {
            $success['workoutPlans'] = WorkoutPlan::where('is_activated', 1)
            ->whereRaw('FIND_IN_SET(?,REPLACE(routine_id,",",","))')
            ->setBindings([1,$routine])
            ->with('workoutSchedule')
            ->get();
         }  
        return response()->json(['status_code' => $this->successStatus , 'message' => '', 'data' => $success]);
    }

    public function workoutsByAgegroup(Request $request)
    {
        $input = $request->input();
        $agegroupId = $request->input('age_group_id');
            
        $success['workoutPlans'] = WorkoutPlan::where('is_activated', 1)
            ->whereRaw('FIND_IN_SET(?,REPLACE(age_group_id,",",","))')
            ->with('workoutSchedule')
            ->setBindings([1, $agegroupId])
            ->get();
    
        return response()->json(['status_code' => $this->successStatus , 'message' => '', 'data' => $success]);
    }
}