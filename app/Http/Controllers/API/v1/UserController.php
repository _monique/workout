<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\User;
use Auth;
use DB;
use Image;
use Illuminate\Support\Facades\Hash;
use App\Inquiry;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Auth\Events\Verified;
use App\CmsPage;
use Illuminate\Support\Facades\Password;
use App\ChildDetail;
use App\Category;
use Illuminate\Validation\Rule;
use App\FcmUser;
use App\Helper;
use App\Notification;
use Carbon\Carbon;
use App\AgeGroup;

class UserController extends Controller
{
    use VerifiesEmails;

    public $successStatus = 200;
    public $errorStatus = 400;

    /**
     * Register api
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {  
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:50',
            'email' => 'required|max:60|unique:users',
            'password' => 'required|confirmed|min:8|max:16',
            'phone_number' => 'required|numeric|unique:users',
        ]);        

        if ($validator->fails())
            return response()->json(['status_code'=> $this->errorStatus, 'message'=> $validator->errors()->first()]);

        $input = $request->all();        	
    
        $input['role_id'] = 2;        
        $input['password'] = bcrypt($input['password']);        
        $user = User::create($input);
        
        DB::table('oauth_access_tokens')
            ->where('user_id', Auth::id())
            ->update([
                'revoked' => 1
            ]);
        
       // $user->sendApiEmailVerificationNotification();

        return response()->json(['status_code' => $this->successStatus, 'message' => 'Please confirm yourself by clicking on verify user button sent to you on your email.','data' => '']);
    }


     /**
     * login api
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',
            'device_type' => [Rule::in(['ios', 'android'])],
            'device_id' => 'required|string',
            'time_zone' => 'required|string',
        ]);

        if ($validator->fails())
            return response()->json(['status_code'=> $this->errorStatus, 'message'=> $validator->errors()->first()]);

        $input = $request->input();

        if (Auth::attempt(['email'=> $input['email'], 'password'=> $input['password'] ]))
        {
            $user = Auth::user();
            $user->update(['time_zone' => $request->time_zone]);
            
            if($user->is_blocked)
                return response()->json(['status_code'=> '999', 'message'=> 'Your account is blocked by admin. Please contact to admin: admin@gmail.com.']);
                
            $input['user_id'] = $user->id;
            $input['fcm_reg_id'] = $request->input('device_id');
            $input['device'] = $request->input('device_type');
            
            FcmUser::where('user_id', $user->id)->delete();
            FcmUser::create($input);
                    
            /*if($user->email_verified_at !== NULL)
            {*/
                DB::table('oauth_access_tokens')
                ->where('user_id', Auth::id())
                ->update([
                    'revoked' => 1
                ]);
                
                $user->profile_image = $user->profile_image != ''? asset('/images/'.$user->profile_image):asset('/images/'.'dummyUser.jpg');
       	    	$success['token'] =  $user->createToken($user->name)->accessToken;
                $success['categories'] = Category::select('id','name')->orderBy('name', 'ASC')->get()->toArray();
       		    $success['userDetails'] = $user;
       		    $success['ageGroups'] = AgeGroup::get()->toArray();

                return response()->json(['status_code' => $this->successStatus, 'message' => 'Login Successfull.', 'data' => $success]);
            /*}else{
                return response()->json(['status_code' => $this->errorStatus, 'message' => ' Please verify your email-id.', 'data' => null]);
            }*/
        } else {
            return response()->json(['status_code' => $this->errorStatus, 'message' => 'You have entered invalid credentials.']);
        }
    }

    /**
     * logout api
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout()
    { 
        if (Auth::check()) {
            FcmUser::whereUserId(Auth::id())->delete();
            Auth::user()->OauthAcessToken()->delete();
            
            return response()->json(['status_code' => $this->successStatus , 'message' => 'Logged out successfully.']);
        } else {
            return response()->json(['status_code' => $this->errorStatus , 'message' => 'Unauthorized user.']);
        }
    }
    
    public function changePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'old_password' => 'required',
            'password' => 'required|min:6',
        ]);
    
        if ($validator->fails())
            return response()->json(['status_code'=> $this->errorStatus, 'message'=> $validator->errors()->first()]);

    	$input = $request->input();
    	
    	 try {
            if ((Hash::check($input['old_password'], Auth::user()->password)) == false) {
                return response()->json(['status_code'=> $this->errorStatus, 'message'=> 'Incorrect old password.']);
            } else {
                User::where('id', Auth::id())->update(['password' => Hash::make($input['password'])]);
                return response()->json(['status_code' => $this->successStatus , 'message' => 'Password updated successfully.']);
            }
        } catch (\Exception $ex) {
            if (isset($ex->errorInfo[2])) {
                $msg = $ex->errorInfo[2];
            } else {
                $msg = $ex->getMessage();
            }
            return response()->json(['status_code'=> $this->errorStatus, 'message'=> $msg]);
        }
    }
    
    public function updateProfilePicture(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'profile_image' => 'required',
        ]);

        if ($validator->fails())
            return response()->json(['status_code'=> 400, 'message'=> $validator->errors()->first(), 'data' => null]);

        $user = Auth::user();
        
        $data = $request->input('profile_image');
        
        $image_parts = explode(";base64,", $data);
        $image_type_aux = explode("image/", $image_parts[0]);
        $image_type = $image_type_aux[1];
        $image_base64 = base64_decode($image_parts[1]);

        $fileName = md5(uniqid(rand(), true));

        $image = $fileName.'.'.$image_type;

        $file = public_path().'/images/'.$image;

        $success = file_put_contents($file, $image_base64);

        $img = Image::make(public_path().'/images/'.$image);

        $user->profile_image = $image;

        if ($user->save()) {
            return response()->json(['status_code' => $this->successStatus , 'message' => 'Profile image updated successfully.', 'data' => asset('/images/'.$user->profile_image)]);
        } else {
            return response()->json(['status_code' => 400 , 'message' => 'Profile image cannot be uploaded. Please try again!', 'data' => null]);
        }
    }
    
    /**
     * edit user profile details api
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function editProfileDetails(Request $request)
    {
        $input = $request->input(); 
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:40',
            'phone_number' => 'min:8|max:15|unique:users,phone_number,'.Auth::id(),
            'height' => 'required',
            'weight' => 'required',
            'child' => 'array',
            'no_of_child' => 'required|numeric'
        ]);        

        if ($validator->fails())
            return response()->json(['status_code'=> $this->errorStatus, 'message'=> $validator->errors()->first(), 'data' => null]);
        $user = Auth::user();
        $user->fill($input);
        $user->save();
        $user->profile_image = $user->profile_image != ''? asset('/images/'.$user->profile_image):'';
        
        $user->childDetails;
        if ($user)
            return response()->json(['status_code' => $this->successStatus , 'message' => 'Profile details updated successfully.', 'data' => $user]);
        else
            return response()->json(['status_code' => $this->errorStatus , 'message' => 'Profile details cannot be updated. Please try again!', 'data' => null]);
    }
    
    public function contactUs(Request $request)
    {
        $input = $request->input();
        
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:40',
            'description' => 'required|max:400',
        ]);
        
        if ($validator->fails())
            return response()->json(['status_code'=> $this->errorStatus, 'message'=> $validator->errors()->first(), 'data' => null]);
            
        $input['user_id'] = Auth::id();
        $inquiry = Inquiry::create($input);

        return response()->json(['status_code' => $this->successStatus , 'message' => 'Inquiry sent successfully.', 'data' => null]);

    }
    
    public function staticPages(Request $request)
    {
        $data = CmsPage::get();
        
        foreach($data as $dat){
            $dat->content = strip_tags(html_entity_decode ($dat->content));
        }
        
        if(!empty($data))
            return response()->json(['status_code' => $this->successStatus , 'message' => '', 'data' => $data]);
        else
            return response()->json(['status_code' => 400 , 'message' => 'Data not found.', 'data' => null]);
    }
    
    public function details(Request $request)
    {
        $user = Auth::user();
        
        if (!empty($user))
        {
            $user->profile_image = ($user->profile_image != '') ? asset('/images/'.$user->profile_image) : asset('/images/'.'dummyUser.jpg');
            $success['childDetails'] = $user->childDetails;
            $success['categories'] = Category::select('id','name')->orderBy('name', 'ASC')->get()->toArray();
            $success['userDetails'] = $user;
            $success['ageGroups'] = AgeGroup::get()->toArray();
            
            return response()->json(['status_code' => $this->successStatus , 'message' => '', 'data' => $success]);
        }
        else
            return response()->json(['status_code' => 400 , 'message' => 'Unauthorized user.', 'data' => null]);
    }
    
    public function addInformation(Request $request)
    {
        $input = $request->input();
        
        $validator = Validator::make($request->all(), [
            'height' => 'required',
            'height_unit' => 'required',
            'weight' => 'required',
            'weight_unit' => 'required',
            'child' => 'array|present|max:5',
            'health_goal' => 'required',
        ]);
        
        if ($validator->fails())
            return response()->json(['status_code'=> $this->errorStatus, 'message'=> $validator->errors()->first(), 'data' => null]);
            
        $user = Auth::user();
        
        if(!empty($input['child']))
        $input['additional_information_added'] = '1';
        $user->fill($input);
        
        if(!empty($input['child'])) {
            ChildDetail::where('user_id',$user->id)->delete();
            foreach($input['child'] as $child ){
                $data['user_id'] = $user->id;
                $data['child_name'] = $child['child_name'];
                $data['child_height'] = $child['child_height'];
                $data['child_height_unit'] = $child['child_height_unit'];
                $data['child_weight'] = $child['child_weight'];
                $data['child_weight_unit'] = $child['child_weight_unit'];
                $data['child_weight_unit'] = $child['child_weight_unit'];
                $data['age_group_id'] = $child['age_group_id'];
                
                ChildDetail::create($data);
            }
        }
        
        if($user->save())
            return response()->json(['status_code' => $this->successStatus , 'message' => 'Information Updated successfully.', 'data' => '' ]);
        else
            return response()->json(['status_code' => $this->errorStatus , 'message' => 'Information can  not be added.' , 'data' => '']);
    }
    
    public function forgot_password(Request $request)
    {
        $input = $request->input();
        
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
        ]);
        
        if ($validator->fails())
            return response()->json(['status_code'=> $this->errorStatus, 'message'=> $validator->errors()->first(), 'data' => null]);
        else {
            try {
                $response = Password::sendResetLink($request->only('email'), function (Message $message) {
                    $message->subject($this->getEmailSubject());
                });
                switch ($response) {
                    case Password::RESET_LINK_SENT:
                        return response()->json(['status_code'=> $this->successStatus, 'message'=> trans($response), 'data' => null]);
                    case Password::INVALID_USER:
                        return response()->json(['status_code'=> $this->errorStatus, 'message'=> trans($response), 'data' => null]);
                }
            } catch (\Swift_TransportException $ex) {
                return response()->json(['status_code'=> $this->errorStatus, 'message'=> $ex->getMessage(), 'data' => null]);
            } catch (Exception $ex) {
                return response()->json(['status_code'=> $this->errorStatus, 'message'=> $ex->getMessage(), 'data' => null]);
            }
        }
    }
    
    public function add_child(Request $request){
        
        $input = $request->input();
        
        $validator = Validator::make($request->all(), [
            'child_name' => 'required',
            'child_height' => 'required',
            'child_height_unit' => 'required',
            'child_weight' => 'required',
            'child_weight_unit' => 'required',
            'age_group_id' => 'required',
        ]);
        
        if ($validator->fails())
            return response()->json(['status_code'=> $this->errorStatus, 'message'=> $validator->errors()->first(), 'data' => null]);

        $childCount = ChildDetail::where('user_id',  Auth::id())->get();
        
        if($childCount->count() >= 5)
         return response()->json(['status_code'=> $this->errorStatus, 'message'=> 'You can add only 5 child details', 'data' => null]);
         
        $input['user_id'] = Auth::id();
        $input['age_group_id'] = $request->age_group_id;
        ChildDetail::create($input);
        
        $child_details = ChildDetail::where('user_id',  Auth::id())->get();
        
        return response()->json(['status_code' => $this->successStatus, 'message' => 'Child added successfully.','data' => $child_details]);
    }
    
    public function edit_child(Request $request){
    
        $input = $request->input();
        
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'child_name' => 'required',
            'child_height' => 'required',
            'child_height_unit' => 'required',
            'child_weight' => 'required',
            'child_weight_unit' => 'required',
             'age_group_id' => 'required',
        ]);
        
        if ($validator->fails())
            return response()->json(['status_code'=> $this->errorStatus, 'message'=> $validator->errors()->first(), 'data' => null]);
            
        $child = ChildDetail::findOrFail($input['id']);
        
        if($child){
            $input['age_group_id'] = $request->age_group_id;
            $child->fill($input);
            $child->save();
            $child_details = ChildDetail::where('user_id',  Auth::id())->get();
            return response()->json(['status_code' => $this->successStatus, 'message' => 'Child details updated successfully.','data' => $child_details]);
        }else{
            return response()->json(['status_code' => $this->errorStatus, 'message' => 'Child can not be edited successfully.','data' => '']);
        }
    }
    
    public function delete_child(Request $request){
    
        $input = $request->input();
        
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);
        
        if ($validator->fails())
            return response()->json(['status_code'=> $this->errorStatus, 'message'=> $validator->errors()->first(), 'data' => null]);
            
        $child = ChildDetail::findOrFail($input['id']);
        $child_details = ChildDetail::where('user_id',  Auth::id())->get();
        
        if($child){
            $child->delete();
            return response()->json(['status_code' => $this->successStatus, 'message' => 'Child deleted successfully.','data' => $child_details]);
        }else{
            return response()->json(['status_code' => $this->errorStatus, 'message' => 'Child can not be deleted successfully.','data' => '']);
        }
    }
    
    public function getNotifications(Request $request)
    {   
        $notifications = Notification::where('user_id', Auth::id())->whereDate('created_at', '>', Carbon::now()->subDays(30))->orderBy('created_at', 'DESC')->get()->toArray();

        Notification::where('user_id', Auth::id())->update(array('is_read' =>  '1'));

        if(count($notifications) > 0){
            return response()->json(['status_code' => $this->successStatus , 'message' => '', 'data' => $notifications]);
        }else{
            return response()->json(['status_code' => $this->errorStatus , 'message' => 'No Notifications', 'data' => null]);
        }
    }

    public function unreadCount(Request $request){

        $unreadNotifcatons = Notification::where('is_read','0')->where('user_id' , Auth::id())->get();
        if($unreadNotifcatons){
            return response()->json(['status_code' => $this->successStatus , 'message' => '','data' => ['unreadNotifications' => count($unreadNotifcatons)]]);
        }else{
            return response()->json(['status_code' => $this->errorStatus , 'message' => '', 'data' => null]);
        }
    }
}
