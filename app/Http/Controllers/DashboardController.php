<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\WorkoutPlan;
use App\Inquiry;
use Carbon\Carbon;
use Session;

class DashboardController extends Controller
{
    public function index(Request $request)
    {

     if (!empty($request->input())) {

        $startDate = Carbon::parse($request->start_date);
        $endDate = Carbon::parse($request->end_date);

        $record = User::select(\DB::raw("COUNT(*) as count"), \DB::raw("MONTHNAME(created_at) as month_name"), \DB::raw("MONTH(created_at) as month"))
                ->whereBetween('created_at', [$startDate, $endDate]) 
                ->groupBy('month_name','month')
                ->orderBy('month')
                ->get();

        $data = [];
 
        foreach($record as $row) {
            $data['label'][] = $row->month_name;
            $data['data'][] = (int) $row->count;
        }
        $chartData['user_data'] = json_encode($data);

        //Workout Plans Year Wise Record
        $record = WorkoutPlan::select(\DB::raw("COUNT(*) as count"), \DB::raw("MONTHNAME(created_at) as month_name"), \DB::raw("MONTH(created_at) as month"))
                ->whereBetween('created_at', [$startDate, $endDate]) 
                ->groupBy('month_name','month')
                ->orderBy('month')
                ->get();
  
        $data = [];
 
        foreach($record as $row) {
            $data['label'][] = $row->month_name;
            $data['data'][] = (int) $row->count;
        }
 
        $chartData['workout_data'] = json_encode($data);
        
        //Contact Us Year Wise Record
        $record = Inquiry::select(\DB::raw("COUNT(*) as count"), \DB::raw("MONTHNAME(created_at) as month_name"), \DB::raw("MONTH(created_at) as month"))
                ->whereBetween('created_at', [$startDate, $endDate]) 
                ->groupBy('month_name','month')
                ->orderBy('month')
                ->get();
  
        $data = [];
 
        foreach($record as $row) {
            $data['label'][] = $row->month_name;
            $data['data'][] = (int) $row->count;
        }
 
        $chartData['contactUs'] = json_encode($data);

        session()->put(['start_date' => $request->input('start_date'), 'end_date' => $request->input('end_date')]);
        
    } else {
        
        //Users Year Wise Record
        $record = User::select(\DB::raw("COUNT(*) as count"), \DB::raw("MONTHNAME(created_at) as month_name"), \DB::raw("MONTH(created_at) as month"))
                ->where('created_at', '>', Carbon::today()->subMonth(12))
                ->groupBy('month_name','month')
                ->orderBy('month')
                ->get();

        $data = [];
 
        foreach($record as $row) {
            $data['label'][] = $row->month_name;
            $data['data'][] = (int) $row->count;
        }
        $chartData['user_data'] = json_encode($data);

        //Workout Plans Year Wise Record
        $record = WorkoutPlan::select(\DB::raw("COUNT(*) as count"), \DB::raw("MONTHNAME(created_at) as month_name"), \DB::raw("MONTH(created_at) as month"))
                ->where('created_at', '>', Carbon::today()->subMonth(12))
                ->groupBy('month_name','month')
                ->orderBy('month')
                ->get();
  
        $data = [];
 
        foreach($record as $row) {
            $data['label'][] = $row->month_name;
            $data['data'][] = (int) $row->count;
        }
 
        $chartData['workout_data'] = json_encode($data);
        
        //Contact Us Year Wise Record
        $record = Inquiry::select(\DB::raw("COUNT(*) as count"), \DB::raw("MONTHNAME(created_at) as month_name"), \DB::raw("MONTH(created_at) as month"))
                ->where('created_at', '>', Carbon::today()->subMonth(12))
                ->groupBy('month_name','month')
                ->orderBy('month')
                ->get();
  
        $data = [];
 
        foreach($record as $row) {
            $data['label'][] = $row->month_name;
            $data['data'][] = (int) $row->count;
        }
 
        $chartData['contactUs'] = json_encode($data);

        Session()->forget(['start_date', 'end_date']);
    }
        
        $totalUsers = User::count();
        
        $newUsers = User::whereYear('created_at', date('Y'))->whereMonth('created_at', date('m'))->count();

    	return view('home', compact('chartData', 'totalUsers', 'newUsers'));
    }
}
