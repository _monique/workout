<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use Validator;
use Image;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::orderBy('updated_at', 'desc')->get();
        return view('categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {  
        $input = $request->input(); 
        $validator =  Validator::make($input,[
            'name' => 'required|string|max:35',
            //'image' => 'required',
            'description' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        
        if ($request->has('image')) {
            $originalImage= $request->file('image');
            $thumbnailImage = Image::make($originalImage);
            $originalPath = public_path().'/categories/';
            $thumbnailImage->save($originalPath.time().$originalImage->getClientOriginalName());

            $input['image'] = time().$originalImage->getClientOriginalName();
        }
        
        $category = new Category();
        $category->name = $input['name'];
        $category->image = $input['image'];
        $category->description = $input['description'];
        $category->save();

        flash()->success('New Category added successfully');
        return redirect()->route('categories.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = Category::findOrFail($id);
        return view('categories.view', compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::findOrFail($id);
        return view('categories.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->input();
        $validator =  Validator::make($input,[
            'name' => 'required|string|max:35',
            'description' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        
        if ($request->has('image')) {
            $originalImage= $request->file('image');
            $thumbnailImage = Image::make($originalImage);
            $originalPath = public_path().'/categories/';
            $thumbnailImage->save($originalPath.time().$originalImage->getClientOriginalName());

            $input['image'] = time().$originalImage->getClientOriginalName();
        }

        $category = Category::findOrFail($id);
        $category->name = $input['name'];
        if ($request->has('image')) 
            $category->image = $input['image'];
        $category->description = $input['description'];
        $category->save();

        flash()->success("Category updated successfully.");
        return redirect()->route('categories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::findOrFail($id);
        if ($category->delete()) {
            $response = array(
                'status' => 'success',
                'message' => 'Category deleted successfully',
            );
        } else {
            $response = array(
                'status' => 'error',
                'message' => 'Category can not be deleted.Please try again',
            );
        }
        return json_encode($response);
    }
}
