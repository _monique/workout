<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CommunityForum;
use App\CommunityForumLike;
use App\ForumComment;
use App\ReportPost;
use App\ReportComment;

class CommunityBoardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $forums = CommunityForum::orderBy('id','DESC')->get();
        return view('forums.index', compact('forums'));
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {       
        $forum = CommunityForum::findOrFail($id);
        $comments = ForumComment::where('forum_id', $id)->get();
        return view('forums.view', compact('forum', 'comments'));
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    { 
        $forum = CommunityForum::findOrFail($id);
        ReportComment::where('forum_id',$id)->delete();
        CommunityForumLike::where('forum_id',$id)->delete();
        ForumComment::where('forum_id',$id)->delete();
        ReportPost::where('forum_id')->delete();
        
        if ($forum->delete()) {
            $response = array(
                'status' => 'success',
                'message' => 'Forum deleted successfully',
            );
        }

        return json_encode($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteComment($id)
    { 
        $comment = ForumComment::findOrFail($id);
        ReportComment::where('comment_id', $id)->delete();
        if ($comment->delete()) {
            $response = array(
                'status' => 'success',
                'message' => 'Forum comment deleted successfully.',
            );
        } else {
            $response = array(
                'status' => 'error',
                'message' => 'Forum comment can not be deleted.Please try again.',
            );
        }

        return json_encode($response);
    }
}
