<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subscription;
use App\Http\Requests\SubscriptionRequest;

class SubscriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subscriptions = Subscription::get();

        return view('subscriptions.index', compact('subscriptions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('subscriptions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SubscriptionRequest $request)
    {
        $input = $request->input();
        $input['plan_period'] = $input['plan_time'].' '.$input['plan_period'];

        $subscription_plan = Subscription::create($input);

        flash()->success('New Subscription Plan added successfully');
        return redirect()->route('subscriptions.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {    
        $subscription = Subscription::findOrFail($id);
        $plan_period = explode(' ', $subscription->plan_period);
        $subscription->plan_time = $plan_period[0];
        $subscription->plan_period = $plan_period[1];
        
        return view('subscriptions.edit', compact('subscription'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SubscriptionRequest $request, $id)
    {
        $input = $request->input(); 
        $input['plan_period'] = $input['plan_time'].' '.$input['plan_period'];

        $subscription = Subscription::findOrFail($id);
        $subscription->fill($input);
        $subscription->save();

        flash()->success('Subscription Plan updated successfully');
        return redirect()->route('subscriptions.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Subscription::findOrFail($id);

        if ($user->delete()) {
            $response = array(
                'status' => 'success',
                'message' => 'Subscription Plan deleted successfully',
            );
        }

        return json_encode($response);
    }
}
