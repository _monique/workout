<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inquiry extends Model
{
    protected $fillable = [
        'user_id','title','description'
    ];
    
    public function user(){
        return $this->belongsTo('App\User','user_id','id');
    }
}
