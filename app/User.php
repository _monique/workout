<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Notifications\VerifyApiEmail;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','role_id','phone_number','email','weight','height','subscription_plan_id','profile_image','no_of_child','health_goal','height_unit','weight_unit','additional_information_added','is_blocked', 'time_zone',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    
    public function OauthAcessToken(){
        return $this->hasMany('App\OauthAccessToken');
    }
    
    public function sendApiEmailVerificationNotification()
    {
        $this->notify(new VerifyApiEmail); // my notification
    }
    
    public function subscriptionPlan(){
        return $this->hasMany('App\UserSubscription', 'user_id', 'id')->orderBy('id', 'DESC')->limit(1);
    }
    
    public function healthGoal(){
        return $this->belongsTo('App\Category', 'health_goal', 'id');
    }
    
    public function childDetails(){
        return $this->hasMany('App\ChildDetail', 'user_id', 'id');
    }
    
    public function notificationKey()
    {
        return $this->hasOne('App\FcmUser');
    }

}
