<?php

namespace App\Exports;

use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class UsersExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return User::select('name','email','phone_number','no_of_child','height','weight','health_goal','is_blocked')->get();
    }
    
    public function headings(): array
    {
        return [
            'Name',
            'Email',
            'Phone Number',
            'No Of Child',
            'Height',
            'Weight',
            'Health Goal',
            'Block Status'
        ];
    }
}
