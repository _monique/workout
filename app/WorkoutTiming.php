<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Auth;

class WorkoutTiming extends Model
{
    protected $fillable = [
        'date','time','user_id','workout_plan_id','time_zone'
    ];
    
    public function getTimeAttribute()
    {
        if  (Auth::user())
            $time =  Carbon::parse($this->attributes['time'])->timezone(Auth::user()->time_zone)->format('h:i A');
        else 
            $time = Carbon::parse($this->attributes['time'])->setTimezone('UTC')->format('h:i A');
            
        return $time;
    }

    public function fcmId(){
        return $this->belongsTo('App\FcmUser','user_id','user_id');
    }
    
    public function user(){
        return $this->belongsTo('App\User','user_id','id');
    }
}
