<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class WorkoutPlan extends Model
{
    protected $fillable = [
        'name','media','short_description','category_id','duration','calories_burn','thumbnail','is_activated','subcategory_id','routine_id','age_group_id'
    ];

    public function category(){
        return $this->belongsTo('App\Category', 'category_id', 'id');
    }
    
    public function workoutSchedule(){
        return $this->hasMany('App\WorkoutTiming', 'workout_plan_id', 'id')->where('user_id', Auth::id());
    }
}
