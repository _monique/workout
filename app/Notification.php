<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Auth;

class Notification extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'title', 'message','is_read','click_action', 'action_id'
    ];

    public function user(){
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function getCreatedAtAttribute()
    {
        return Carbon::parse($this->attributes['created_at'])->timezone(Auth::user()->time_zone)->format('d/m/Y h:i A');
    }

    public static function saveNotification($user_id, $title, $message, $click_action, $action_id= null)
    {
		$notification = new Notification();
		$notification['user_id'] = $user_id;
		$notification['title'] = $title;
		$notification['message'] = $message;
		$notification['click_action'] = $click_action;
		$notification['action_id'] = $action_id;
		$notification->save();
    }
}
