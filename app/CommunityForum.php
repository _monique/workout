<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class CommunityForum extends Model
{
    public $timestamps = false;
    
    protected $fillable = [
        'user_id','title','media','description','created_at','updated_at','is_reported'
    ];
    
    public function is_liked(){
        return $this->hasOne('App\CommunityForumLike','forum_id','id')->where('user_id', Auth::id());
    }
    
    public function totalLikes(){
        return $this->hasMany('App\CommunityForumLike','forum_id','id');
    }
    
    public function comments(){
        return $this->hasMany('App\ForumComment','forum_id','id');
    }
    
    public function user(){
        return $this->belongsTo('App\User','user_id','id')->select('id','name','profile_image');
    }
    
    public function fcmId(){
        return $this->belongsTo('App\FcmUser','user_id','user_id');
    }

}
