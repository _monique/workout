<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSubscription extends Model
{
    protected $fillable = [
        'user_id', 'production_id', 'transaction_id', 'name', 'validity','price', 'purchase_date', 'end_date', 'device_type', 'is_autorenewal',
    ];
}
