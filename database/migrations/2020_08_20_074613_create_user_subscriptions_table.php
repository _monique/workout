<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_subscriptions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->string('production_id');
            $table->string('transaction_id');
            $table->string('name');
            $table->string('validity');
            $table->string('purchase_date');
            $table->string('end_date')->nullable();
            $table->enum('device_type',['android','ios'])->nullable();
            $table->boolean('is_autorenewal')->default(0);
            $table->timestamps();
        });

        Schema::table('user_subscriptions', function(Blueprint $table){
            $table->foreign('user_id')->references('id')->on('users')->omDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_subscriptions' ,function(Blueprint $table){
            $table->dropForeign('user_subscriptions_user_id_foreign');
            $table->dropColumn('user_id');
        });

        Schema::dropIfExists('user_subscriptions');
    }
}
