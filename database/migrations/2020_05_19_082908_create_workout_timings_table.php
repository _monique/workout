<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkoutTimingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('workout_timings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('date');
            $table->string('time');
            $table->string('time_zone');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('workout_plan_id');
            $table->timestamps();
        });
        
        Schema::table('workout_timings', function(Blueprint $table){
            $table->foreign('user_id')->references('id')->on('users')->omDelete('cascade');
            $table->foreign('workout_plan_id')->references('id')->on('workout_plans')->omDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('workout_timings' ,function(Blueprint $table){
            $table->dropForeign('workout_timings_user_id_foreign');
            $table->dropForeign('workout_timings_workout_plan_id_foreign');
            $table->dropColumn('user_id');
            $table->dropColumn('workout_plan_id');
        });
        Schema::dropIfExists('workout_timings');
    }
}
