<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('role_id');
            $table->string('name');
            $table->string('phone_number');
            $table->string('email')->unique();
            $table->string('height')->nullable();
            $table->string('weight')->nullable();
            $table->string('height_unit')->nullable();
            $table->string('weight_unit')->nullable();
            $table->string('profile_image')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('no_of_child')->nullable();
            $table->string('health_goal')->nullable();
            $table->unsignedBigInteger('subscription_plan_id')->nullable();
            $table->boolean('additional_information_added')->nullable();
            $table->boolean('is_blocked')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });

        Schema::table('users', function(Blueprint $table){
            $table->foreign('role_id')->references('id')->on('roles')->omDelete('cascade');
            $table->foreign('subscription_plan_id')->references('id')->on('subscriptions')->omDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users' ,function(Blueprint $table){
            $table->dropForeign('users_role_id_foreign');
            $table->dropForeign('users_subscription_plan_id_foreign');
            $table->dropColumn('role_id');
            $table->dropColumn('subscription_plan_id');
        });
        Schema::dropIfExists('users');
    }
}
