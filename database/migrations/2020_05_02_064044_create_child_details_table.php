<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChildDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('child_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->string('child_name');
            $table->string('child_height');
            $table->string('child_height_unit');
            $table->string('child_weight');
            $table->string('child_weight_unit');
            $table->timestamps();
        });
        
        Schema::table('child_details', function(Blueprint $table){
            $table->foreign('user_id')->references('id')->on('users')->omDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('child_details' ,function(Blueprint $table){
            $table->dropForeign('child_details_user_id_foreign');
            $table->dropColumn('user_id');
        });
        Schema::dropIfExists('child_details');
    }
}
