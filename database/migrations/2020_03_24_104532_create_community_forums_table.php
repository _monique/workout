<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommunityForumsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('community_forums', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('media');
            $table->text('description');
            $table->unsignedBigInteger('user_id');
            $table->boolean('is_reported');
            $table->timestamps();
        });
        
        Schema::table('community_forums', function(Blueprint $table){
            $table->foreign('user_id')->references('id')->on('users')->omDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('community_forums' ,function(Blueprint $table){
            $table->dropForeign('community_forums_user_id_foreign');
            $table->dropColumn('user_id');
        });
        Schema::dropIfExists('community_forums');
    }
}
