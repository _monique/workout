<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_comments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('forum_id');
            $table->unsignedBigInteger('comment_id');
            $table->unsignedBigInteger('user_id');
            $table->string('reason', 255);
            $table->boolean('is_reported')->default(0);
            $table->timestamps();
        });

        Schema::table('report_comments', function(Blueprint $table){
            $table->foreign('forum_id')->references('id')->on('community_forums')->omDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->omDelete('cascade');
            $table->foreign('comment_id')->references('id')->on('forum_comments')->omDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('report_comments' ,function(Blueprint $table){
            $table->dropForeign('report_comments_user_id_foreign');
            $table->dropForeign('report_comments_forum_id_foreign');
            $table->dropForeign('report_comments_comment_id_foreign');
            $table->dropColumn('user_id');
            $table->dropColumn('forum_id');
            $table->dropColumn('comment_id');
        });
        Schema::dropIfExists('report_comments');
    }
}
