<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->string('title');
            $table->string('message');
            $table->boolean('is_read');
            $table->string('click_action');
            $table->integer('action_id');
            $table->timestamps();
        });
        
        Schema::table('notifications', function(Blueprint $table){
            $table->foreign('user_id')->references('id')->on('users')->omDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notifications' ,function(Blueprint $table){
            $table->dropForeign('notifications_user_id_foreign');
            $table->dropColumn('user_id');
        });
        Schema::dropIfExists('notifications');
    }
}
