<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommunityForumLikesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('community_forum_likes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('like');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('forum_id');
            $table->timestamps();
        });
        
        Schema::table('community_forum_likes', function(Blueprint $table){
            $table->foreign('user_id')->references('id')->on('users')->omDelete('cascade');
            $table->foreign('forum_id')->references('id')->on('community_forums')->omDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('community_forum_likes' ,function(Blueprint $table){
            $table->dropForeign('community_forum_likes_user_id_foreign');
            $table->dropForeign('community_forum_likes_forum_id_foreign');
            $table->dropColumn('user_id');
            $table->dropColumn('forum_id');
        });
        Schema::dropIfExists('community_forum_likes');
    }
}
