<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_posts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('forum_id');
            $table->string('reason');
            $table->timestamps();
        });
        
        Schema::table('report_posts', function(Blueprint $table){
            $table->foreign('user_id')->references('id')->on('users')->omDelete('cascade');
            $table->foreign('forum_id')->references('id')->on('community_forums')->omDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('report_posts' ,function(Blueprint $table){
            $table->dropForeign('report_posts_user_id_foreign');
            $table->dropForeign('report_posts_forum_id_foreign');
            $table->dropColumn('user_id');
            $table->dropColumn('forum_id');
        });
        Schema::dropIfExists('report_posts');
    }
}
