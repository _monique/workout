<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateForumCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forum_comments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('forum_id');
            $table->text('comment');
            $table->timestamps();
        });
        
        Schema::table('forum_comments', function(Blueprint $table){
            $table->foreign('user_id')->references('id')->on('users')->omDelete('cascade');
            $table->foreign('forum_id')->references('id')->on('community_forums')->omDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('forum_comments' ,function(Blueprint $table){
            $table->dropForeign('forum_comments_user_id_foreign');
            $table->dropForeign('forum_comments_forum_id_foreign');
            $table->dropColumn('user_id');
            $table->dropColumn('forum_id');
        });

        Schema::dropIfExists('forum_comments');
    }
}
