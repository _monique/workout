<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkoutPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('workout_plans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('media');
            $table->string('thumbnail');
            $table->string('category_id')->nullable();
            $table->string('subcategory_id')->nullable();
            $table->string('routine_id')->nullable();
            $table->string('age_group_id')->nullable();
            $table->string('short_description');
            $table->string('duration')->nullable();
            $table->string('calories_burn')->nullable();
            $table->boolean('is_activated')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('workout_plans');
    }
}
