<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubcategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subcategories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('category_id');
            $table->string('name');
            $table->string('image');
            $table->timestamps();
        });
        Schema::table('subcategories', function(Blueprint $table){
            $table->foreign('category_id')->references('id')->on('categories')->omDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subcategories' ,function(Blueprint $table){
            $table->dropForeign('subcategories_category_id_foreign');
            $table->dropColumn('category_id');
        });
        Schema::dropIfExists('subcategories');
    }
}
