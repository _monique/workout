<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFcmUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fcm_users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->text('fcm_reg_id')->nullable();
            $table->enum('device',['android','ios'])->nullable();
            $table->timestamps();
        });
        
        Schema::table('fcm_users', function(Blueprint $table){
            $table->foreign('user_id')->references('id')->on('users')->omDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fcm_users' ,function(Blueprint $table){
            $table->dropForeign('fcm_users_user_id_foreign');
            $table->dropColumn('user_id');
        });
        Schema::dropIfExists('fcm_users');
    }
}
