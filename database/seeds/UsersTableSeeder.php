<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        DB::table('users')->insert([[
                'role_id' => '1',
                'name' => 'Admin',
                'phone_number' => '9999999090',
                'email' => 'admin@gmail.com',
                'password' => bcrypt('secret'),
                'created_at' => Carbon::now()
            ]
        ]);
    }
}
