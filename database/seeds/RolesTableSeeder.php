<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->delete();

        DB::table('roles')->insert([
        	[
        		'name' => 'Admin',
        		'created_at' => Carbon::now(),
        	],[
        		'name' => 'User',
        		'created_ate' => Carbon::now(),
        	]
        ]);
    }
}
